#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################

import argparse
parser = argparse.ArgumentParser(description="Change PROFUMO version number.")
parser.add_argument("version")
args = parser.parse_args()

if args.version == "dev":
    dev = True
    print("Switching to development version.")
else:
    dev = False
    print("Switching to version {v}.".format(v=args.version))

################################################################################

import os
import shutil
import datetime

################################################################################

# PROFUMO.c++

shutil.copy("PROFUMO.c++", "PROFUMO.c++.bak")


contents = []
with open("PROFUMO.c++", "r") as inputFile:
    for line in inputFile:
        # TCLAP printing
        if "TCLAP::CmdLine cmd" in line:
            if dev:
                versionString = "Development version"
            else:
                versionString = args.version
            line = "        TCLAP::CmdLine cmd(\"PROFUMO: infers PRObabilistic FUnctional MOdes from fMRI data.\", \' \', \"{v}\");\n".format(v=versionString)
        # JSON config
        if "config[\"> Version\"]" in line:
            if dev:
                versionString = "Development version"
            else:
                versionString = "Version " + args.version
            line = "    config[\"> Version\"] = \"{v}\";\n".format(v=versionString)
        # Store!
        contents.append(line)


with open("PROFUMO.c++", "w") as outputFile:
    for line in contents:
        outputFile.write(line)


os.remove("PROFUMO.c++.bak")

################################################################################

# README.md

if not dev:
    
    shutil.copy("README.md", "README.md.bak")
    
    
    contents = []
    with open("README.md", "r") as inputFile:
        for line in inputFile:
            # Header
            if "*Version" in line:
                line = "*Version {v}* --- {d}\n".format(v=args.version, d=datetime.date.today().strftime("%Y-%m-%d"))
            # Store!
            contents.append(line)


    with open("README.md", "w") as outputFile:
        for line in contents:
            outputFile.write(line)


    os.remove("README.md.bak")

################################################################################

# Makefile
    
shutil.copy("Makefile", "Makefile.bak")


contents = []
with open("Makefile", "r") as inputFile:
    for line in inputFile:
        # install
        if "install: PROFUMO" in line:
            contents.append(line)
            if dev:
                line = "\tcp PROFUMO ${HOME}/bin/PROFUMO_dev_$(TIMESTAMP)\n"
                contents.append(line)
                line = "\tln -sf PROFUMO_dev_$(TIMESTAMP) ${HOME}/bin/PROFUMO_dev\n"
                contents.append(line)
            else:
                line = "\tcp PROFUMO ${{HOME}}/bin/PROFUMO_v{v}_$(TIMESTAMP)\n".format(v=args.version)
                contents.append(line)
                line = "\tln -sf PROFUMO_v{v}_$(TIMESTAMP) ${{HOME}}/bin/PROFUMO_v{v}\n".format(v=args.version)
                contents.append(line)
                line = "\tln -sf PROFUMO_v{v} ${{HOME}}/bin/PROFUMO\n".format(v=args.version)
                contents.append(line)
            # Skip to blank line (this section can be two or three lines)
            while next(inputFile).strip():
                pass
            line = ("\n")
            
        # uninstall
        if "uninstall:" in line:
            contents.append(line); line = next(inputFile)
            if dev:
                line = "\trm -f ${HOME}/bin/PROFUMO_dev*\n"
            else:
                line = "\trm -f ${{HOME}}/bin/PROFUMO_v{v}_* ${{HOME}}/bin/PROFUMO_v{v} ${{HOME}}/bin/PROFUMO\n".format(v=args.version)
        # Store!
        contents.append(line)


with open("Makefile", "w") as outputFile:
    for line in contents:
        outputFile.write(line)


os.remove("Makefile.bak")


################################################################################


