function [T] = ssglmT(Y,X)

pinvX=pinv(X);

if size(X,1)<100
  R=eye(size(X,1))-X*pinvX;
  trR=trace(R);
  nu=round(trace(R)^2/trace(R*R));
else
  nu=size(X,1)-size(X,2);
  trR=nu;
end

pe=pinvX*Y;
size(pe)
res=Y-X*pe;
size(Y)
size(X)
size(res)
sigsq=sum(res.^2)/trR;
size(sigsq)

varcope=diag(pinvX*pinvX')*sigsq;
size(diag(pinvX*pinvX'))
size(varcope)

T = pe ./ sqrt(varcope);

