// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - test stuff 
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */
// run like this:  
// $ cd ~rezvanh/PROFUMO
// $ scl enable devtoolset-6 bash
// $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/fs0/rezvanh/lib
// line below or just $make (it will update the modified files)
// $ make unistall then $ make clean then $ make then $ make all then $ make install
// ./main_test /home/fs0/rezvanh/HCP_Profumo_cifti_goodsubs200/DataLocations3.json /vols/Scratch/rezvanh/test_rebuilding/ 20 -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain_mask_dil1.nii.gz

#include <map>
#include <memory>
#include <string>
#include <iostream>
#include <iomanip>    // std::put_time
#include <fstream>
#include <sstream>    // std::ostringstream
#include <sys/stat.h>
#include <locale>     // std::locale, std::isspace, std::isdigit
#include <armadillo>
#include <algorithm>
#include <omp.h>
#include <stdexcept>
#include <chrono>     // std::system_clock
#include <ctime>      // std::time_t, std::localtime
#include <tclap/CmdLine.h>
#include <json.hpp>
#include <vector>
#include <typeinfo>
using json = nlohmann::json;
#include "/home/fs0/rezvanh/PROFUMO/Source/Utilities/DataIO.h" 
#include "/home/fs0/rezvanh/PROFUMO/Source/DataTypes.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/DataLoader.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/DataLoaders/FullRankDataLoader.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/DataLoaders/LowRankDataLoader.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/Utilities/RandomSVD.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/Utilities/DataNormalisation.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/SubjectModelling/Run.h"
#include "/home/fs0/rezvanh/PROFUMO/Source/SubjectModelling/Subject.h"


// #include "/home/fs0/rezvanh/PROFUMO/Source/Utilities/DataNormalisation.h"
struct Arguments
{
public:
    std::string dataLocationsFile;
    unsigned int M;
    std::string outputDir;
    
    bool useMask;
    std::string maskFile;
    bool useFixedParameters;
    bool useGroupMeans;
    std::string groupMeansFile;
    bool useGroupPrecisions;
    std::string groupPrecisionsFile;
    bool useGroupMemberships;
    std::string groupMembershipsFile;
    bool useSpatialNeighbours;
    std::string spatialNeighboursFile;
    bool useHRF;
    float TR;
    std::string hrfFile;
    unsigned int K;
    float dof;
    bool normaliseData;
    bool bigData;
    unsigned int multiStartIterations; 
};

int main(int argc, const char **argv) {
    Arguments args;
    try {
    
        const std::string EMPTY_DEFAULT = "**DEFAULT**";
            
        // Set everything up (description, delimiter, version)
        TCLAP::CmdLine cmd("PROFUMO: infers PRObabilistic FUnctional MOdes from fMRI data.", ' ', "Development version");
        
        // Add the required arguments
        // (name, description, required, default, type)
        // N.B. The help message is more useful if type = name
        TCLAP::UnlabeledValueArg<std::string> dataLocationsFile("DataLocations.json", "File containing data locations in JSON format.", true, EMPTY_DEFAULT, "DataLocations.json");
        cmd.add( dataLocationsFile );
        
        TCLAP::UnlabeledValueArg<std::string> outputDir("outputDirectory", "Output directory.", true, EMPTY_DEFAULT, "outputDirectory");
        cmd.add( outputDir );
        
        TCLAP::UnlabeledValueArg<int> M("M", "Number of modes to infer.", true, 0, "M");
        cmd.add( M );
        
        TCLAP::ValueArg<std::string> maskFile("m", "mask", "Use to mask the raw data / any pre-specified parameters.", false, EMPTY_DEFAULT, "file");
        cmd.add( maskFile );
        
        cmd.parse( argc, argv );
        // std::cout << argv[0] << std::endl;
        // Add the required arguments
        // (name, description, required, default, type)
        // N.B. The help message is more useful if type = name
        args.dataLocationsFile = dataLocationsFile.getValue();
        args.outputDir = outputDir.getValue();
        args.maskFile = maskFile.getValue();
        
        // Modes
        const int iM = M.getValue();
        if ( iM > 0 ) { args.M = (unsigned int) iM; }
        else { throw std::invalid_argument("Number of modes (M) must be a positive integer."); }
        
    }
    catch (TCLAP::ArgException &e)  // Catch any exceptions
    {
        std::cerr << "Error: " << e.error() << " for arg " << e.argId() << std::endl;
        return -1;
    }
    const std::string bigDataDir= args.outputDir + "BigDataInitialDir/";
    mkdir( bigDataDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); 
   
    std::cout << "Hello!" << std::endl;
    std::cout << args.M << std::endl;
    std::cout << args.dataLocationsFile << std::endl;
    std::shared_ptr<PROFUMO::DataLoaders::LowRankDataLoader> LRDLoader;
    //std::shared_ptr<PROFUMO::DataLoaders::FullRankDataLoader> FRDLoader;
    std::shared_ptr<const arma::uvec> maskInds = nullptr;
    LRDLoader = std::make_shared<PROFUMO::DataLoaders::LowRankDataLoader>( args.dataLocationsFile, args.outputDir, args.M, 200, maskInds, true, true, false, bigDataDir );
    //FRDLoader = std::make_shared<PROFUMO::DataLoaders::FullRankDataLoader>( args.dataLocationsFile, args.outputDir, args.M, maskInds, true, true, false, bigDataDir );
    const arma::fmat maskData = PROFUMO::Utilities::loadDataMatrix( args.maskFile );
    int S_ = LRDLoader->getS();
    std::cout << S_ <<std::endl;
    arma::fmat spatialBasis = LRDLoader->computeSpatialBasis( 2 * args.M, true, bigDataDir );
    
    char foo [] = { 'a', 'v', 's', 'e', 'f' };
    char foo1 [3];
    for (int jj=0; jj<3; ++jj) {
    foo1[jj]=foo[jj];
    std::cout << foo1[jj] << std::endl;
    
    }
    //const std::map<SubjectID, std::map<RunID, D>> subjectData
    //subjectData=LRDLoader->getData()
    
    
    //std::map<PROFUMO::SubjectID, std::map<PROFUMO::RunID, std::string>> dataLocations2=LRDLoader->getDataLoc();//const std::map<PROFUMO::SubjectID, std::map<PROFUMO::RunID, std::string>> dataLocations2 = PROFUMO::DataLoader<LRDLoader>::loadDataLocations();
    //std::vector<PROFUMO::SubjectID> subjectList = LRDLoader->getSubjectList();
    std::vector<PROFUMO::SubjectID> subjectList;
    for (const auto& subject : LRDLoader->getData()) {
        subjectList.emplace_back(subject.first);
        
    }
    std::cout << subjectList.size() << std::endl;
    unsigned int s;
    std::string subjectDirectory;
    std::vector<std::string> listall;
    for (s = 0; s < subjectList.size(); ++s) {
        PROFUMO::SubjectID subjectID = subjectList[s];  
        subjectDirectory = "test/" + subjectID + "/";
        std::cout << s << std::endl;
        std::cout << subjectDirectory << std::endl; 
        listall.push_back(subjectDirectory);     
        // Load each run
        //for (const auto& run : dataLocations.at(subjectID)) {
        //    const PROFUMO::RunID runID = run.first;
        //    const std::string dataLocation = run.second;

        //    std::cout << dataLocation << std::endl;
        //}
    }
    std::cout << listall.size() << std::endl;
    //for (unsigned int s = 0; s < subjectList.size(); ++s) {
    //    const PROFUMO::SubjectID subjectID = subjectList[s];            
        //const std::string subjectDirectory = "test/" + subjectID + "/";
        //std::cout << s << std::endl;
        //std::cout << subjectDirectory << std::endl;
    //}
    
    //for (std::map<PROFUMO::SubjectID, std::map<PROFUMO::RunID, std::string>>::const_iterator subject = dataLocations2.begin(); subject != dataLocations2.end(); ++subject) {//for(auto it : dataLocations2)//(auto it = dataLocations2.begin(); it != dataLocations2.end(); ++it)
    //    std::string subjectID = subject->first;
    //    std::cout << subjectID << "\n";
    //}
    
    //std::cout << typeof(dataLocations2) << std::endl;
     
    
    arma::fmat Y;
    Y = arma::zeros<arma::fmat>(10, 100);
    arma::fmat Y1;
    Y1 = Y.t();
    //std::cout << Y1.n_rows << std::endl;
    //std::cout << Y1.n_cols << std::endl;
    
    // int thisint1=1;
    // int thisint2=thisint1;
    // thisint1=2;
    // std::cout << thisint2 << std::endl;
    // arma::fmat datasub
    //arma::fmat datasub;
    // datasub = arma::randn<arma::fmat>(10, 20);
    //for (int ii; ii<5; ++ii){
    //datasub = arma::join_rows(datasub,arma::randn<arma::fmat>(10, 20));
    //}
    //std::cout << datasub.n_rows << std::endl;
    //std::cout << datasub.n_cols << std::endl;
    // std::cout << datasub << std::endl;
    //arma::fvec Thiseigval;
    //arma::fmat Thiseigvec;
    //arma::eig_sym( Thiseigval, Thiseigvec, datasub*datasub.t());
    //std::cout << Thiseigvec.n_rows << std::endl;
    //std::cout << Thiseigvec.n_cols << std::endl;
    //std::cout << Thiseigvec << std::endl;
    //std::cout << Thiseigval << std::endl;
    
    arma::sp_mat A = arma::sprandu<arma::sp_mat>(10, 10, 0.1);
    arma::cx_vec eigval;
    arma::cx_mat eigvec;
    
    eigs_gen(eigval, eigvec, A, 5);  // find 5 eigenvalues/eigenvectors
    //std::cout << eigvec << std::endl;
    //std::cout << eigvec.n_rows << std::endl;
    //std::cout << eigvec.n_cols << std::endl;
    
    eigs_gen(eigval, eigvec, A, 6);  // find 5 eigenvalues/eigenvectors
    
    const std::string outdir = "/vols/Scratch/rezvanh/test_row.hdf5";
    //*pch=arma::zeros<arma::fmat>(2,1);
    //pch->save(outdir, arma::hdf5_binary);
    //int thisn=1;
    //std::shared_ptr<arma::fvec> aaa = std::make_shared<arma::fvec>({ 1, 2.0 });
    arma::fvec aaa = { 1, 2.0 };
    //std::shared_ptr<arma::fmat> aaa;
    //aaa->insert_rows(1,4);
    //aaa->insert_rows(2,5);
    aaa.save(outdir, arma::hdf5_binary);
    arma::fvec bbb;
    bbb.load(outdir, arma::hdf5_binary);
    std::cout << bbb.n_cols <<std::endl;
    unsigned int Nl_ = bbb.at(0); //arma::conv_to<int>::  //arma::from(N_TrDtD[0]);
    const float TrDtDl = bbb.at(1);
    //
    std::cout << Nl_ << std::endl;
    std::cout << TrDtDl << std::endl;
    /*
    unsigned int nsubs=10;
    unsigned int batchNum=4;
    unsigned int batchSize=3;
    double Ti=0.9;
    arma::vec subProbs = arma::ones<arma::vec>(nsubs);
    //std::cout << subProbs << std::endl;
    arma::mat subPicked;
    subPicked = arma::ones<arma::mat>(nsubs,1);
    arma::vec subCoef = arma::round(100*arma::pow(subPicked,Ti));
    //std::cout << subCoef << std::endl;
    arma::vec linOrder = arma::linspace<arma::vec>(0, nsubs-1, nsubs);
    arma::vec linOrderProb = linOrder;
    arma::vec rx = arma::join_vert(linOrder,linOrderProb);
    std::cout << rx << std::endl;
    arma::vec randOrder;
    arma::vec randOrderBatch;
    for (unsigned int batchcnt=0; batchcnt<batchNum; ++batchcnt) {
        randOrder = arma::shuffle(linOrder);
        randOrderBatch = randOrder.head(batchSize);
        // do stuff
        //std::cout << randOrder[randOrder[0]] << std::endl;
        //std::cout << randOrder << std::endl;
        subPicked.rows(randOrderBatch)=2.0;//.at(randOrderBatch)+=1;//
        linOrderProb.reset();
    }
    */
    
    
    // TCLAP::UnlabeledValueArg<int> M("M", "Number of modes to infer.", true, 0, "M");
    // cmd.add( M );
    // std::cout << M;
    
    // TCLAP::SwitchArg noNormalisation("", "noDataNormalisation", "BE VERY CAREFUL! This turns off the internal data normalisation, which scales the data such that it matches the generative model. If you are confident in your own preprocessing this can be disabled here...", cmd, false); 

    return 0;
}

// const arma::fmat maskData = PROFUMO::Utilities::loadDataMatrix( args.maskFile );
// options.maskInds = std::make_shared<const arma::uvec>(arma::find( maskData != 0.0 )); 
// run like this:  
// cd to ~rezvanh/PROFUMO
// $ scl enable devtoolset-6 bash
// $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/fs0/rezvanh/lib
// line below or just $make (it will update the modified files)
// $ make unistall then $ make clean then $ make then $ make all then $ make install
// ./main_test /home/fs0/rezvanh/HCP_Profumo_cifti_goodsubs200/DataLocations1.json /vols/Scratch/rezvanh/test_rebuilding 20 -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain_mask_dil1.nii.gz

 

 