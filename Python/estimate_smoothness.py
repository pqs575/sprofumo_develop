#!/usr/bin/env python
# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2017
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# estimate_smoothness.py
# Calculates `--dofCorrection` argument for PROFUMO for *volumetric* data

###############################################################################

import os, os.path as op
import argparse
import subprocess

import math
import numpy, numpy.linalg
import scipy, scipy.stats

import nibabel

import matplotlib as mpl
import matplotlib.pyplot as plt

import profumo.utilities as utils
import profumo.io as io
import profumo.plotting as pplt

###############################################################################

parser = argparse.ArgumentParser(description=(
    "Estimate degrees of freedom correction parameter for fMRI data."))

parser.add_argument(
        "data", type=utils.valid_file, help="Input data. Must be a NIFTI file.")

parser.add_argument(
        "mask", type=utils.valid_file, help="Mask to apply to data.")

args = parser.parse_args()

###############################################################################
print("-" * 80)
print("Loading data...")
print()

# Default: Nibabel now recommends get_fdata() over get_data()
# http://nipy.org/nibabel/images_and_memory.html#use-the-caching-keyword-to-get-fdata

print("Data: " + args.data)
data = nibabel.load(args.data)
if not isinstance(data, io.NIFTI):
    raise TypeError("Data must be a NIFTI file!")
print("Mask: " + args.mask)
mask = nibabel.load(args.mask)
mask_voxels = mask.get_fdata(caching='unchanged') != 0.0
print()

# Unmask
print("Unmasking and normalising data...")
masked_data = data.get_fdata(caching='unchanged')[mask_voxels]

n_v, n_t = masked_data.shape
print("Voxels: {:d}".format(n_v))
print("Time points: {:d}".format(n_t))

# Normalise
masked_data = scipy.stats.mstats.zscore(masked_data, axis=1)
masked_data = numpy.nan_to_num(masked_data)

fig, ax = plt.subplots()
pplt.plot_matrix(
        fig, ax, masked_data, title="Masked data",
        ylabel="Voxel", xlabel="Time point", clabel=None)
plt.show()

print("Done.")
print()
###############################################################################
print("-" * 80)
print("Estimating residuals...")
print()


print("Calculating SVD...")
U,s,V = numpy.linalg.svd(masked_data, full_matrices=False)
print("Done.")
print()

plt.figure()
plt.plot(s)
plt.xlabel("Component"); plt.ylabel("Singular value")
plt.show()


# Get how many components to remove
N = int(input("Number of signal components to remove: "))
print("N = {:d}".format(N))
print()

# Form the residuals
masked_residuals = U[:,N:] @ numpy.diag(s[N:]) @ V[:,N:].T

# Clean up
del U, s, V

fig, ax = plt.subplots()
pplt.plot_matrix(
        fig, ax, masked_residuals, title="Masked residuals",
        ylabel="Voxel", xlabel="Time point", clabel=None)
plt.show()


print("Saving residuals...")

# Unmask
residuals = numpy.zeros( data.shape )
residuals[mask_voxels] = masked_residuals

# Write to file
residuals = type(data)(residuals, affine=data.affine, header=data.header)
residual_file = '/tmp/Residuals.nii.gz'
nibabel.save(residuals, residual_file)

# Clean up - reload image such that we switch to an array proxy for the data
# http://nipy.org/nibabel/nibabel_images.html#array-proxies
residuals = nibabel.load(residual_file)

print("Done.")
print()

###############################################################################
print("-" * 80)
print("Estimating temporal DoF...")
print()

# Method 1: just the rank remaining after FIX / signal removal
#tDoF = numpy.linalg.matrix_rank(masked_residuals)

# Method 2: Depends on temporal covariance matrix
# Groves et al., "Linked independent component analysis for multimodal data fusion", NeuroImage, 2011 (Appendix A)
# DOI: 10.1016/j.neuroimage.2010.09.073
#KKt = masked_residuals.T @ masked_residuals
#tDoF = numpy.trace(KKt)**2 / numpy.trace(KKt @ KKt)

# Method 3:
# Woolrich et al., "Temporal Autocorrelation in Univariate Linear Modeling of FMRI Data", NeuroImage, 2001
# DOI: 10.1006/nimg.2001.0931
# Equation 3 with S = I; X = ones(T,1); c = 1
# i.e. keff = sum(V)

# Method 4: Fit AR model and use the equation from method 2
output = subprocess.run(
        ['fslmaths', residual_file, '-Tar1',
            '-mas', args.mask, '/tmp/AR.nii.gz'
        ], check=True, stdout=subprocess.PIPE)
output = subprocess.run(
        ['fslstats', '/tmp/AR.nii.gz', '-a', '-M'],
        check=True, stdout=subprocess.PIPE)
alpha = float(output.stdout)

# Covariance matrix:
# en.wikipedia.org/wiki/Autoregressive_model
# C(i,j) = sigma**2 / (1 - alpha**2) * alpha**(abs(i-j))
# Normalise to correlation for simplicity:
# C(i,j) = alpha**(abs(i-j))

# Adrian's paper
# dof = Tr[KKt]**2 / Tr[KKtKKt]
# In our case, C = K @ Kt
# dof = T**2 / sum(C * C)
T = n_t
denom = T
for t in range(1,T):
    denom += 2 * (T - t) * alpha**(2*t)
t_dof = T ** 2 / denom


t_dof_correction = t_dof / n_t

print("Time points: {:d}".format(n_t))
print("Rank raw data: {:d}".format(numpy.linalg.matrix_rank(masked_data)))
print("Rank residuals: {:d}".format(numpy.linalg.matrix_rank(masked_residuals)))
print("Temporal degrees of freedom: {:.2f}".format(t_dof))
print("tDoF correction: {:.4f}".format(t_dof_correction))

print("Done.")
print()
###############################################################################
print("-" * 80)
print("Estimating spatial DoF...")
print()

# Spatial DoF
smoothest = subprocess.run(
        ['smoothest', '-d', '{:.2f}'.format(t_dof),
            '-m', args.mask, '-r', residual_file
        ], check=True, stdout=subprocess.PIPE)

# http://blogs.warwick.ac.uk/nichols/entry/fwhm_resel_details/
# The RESEL output from smoothest is the size of one RESEL in voxel units
for line in smoothest.stdout.splitlines():
    if 'RESELS' in str(line):
        resel_size = float(line.split()[-1])

# From Adrian's paper (K = number of spatial dimensions):
# dofCorrection = (nRESELS / n_v) * (4 * log(2) / pi) ** (K/2)
#               = (nRESELS / n_v) * 0.83    (for 3D data)
#               = 0.83 / smoothest_RESELS

n_resels = n_v / resel_size
s_dof_correction = 0.83 / resel_size

print("Voxels: {:d}".format(n_v))
print("Resels: {:.2f}".format(n_resels))
print("sDoF correction: {:.4f}".format(s_dof_correction))
print()

# Recommendation for PROFUMO
# Fudge! Don't be so harsh if there is temporal smoothness too
s_dof_param = s_dof_correction / t_dof_correction
print("sDoF corr / tDoF corr: {:.4f}".format(s_dof_param))
print()

# And round up to a nice number
def next_highest(seq, x):
    return min([(i-x,i) for i in seq if x<=i] or [(0,None)])[1]

def round_power(x):
    a = math.log10(x); b = a % 1; a = a - b
    b = next_highest(numpy.log10([1.0,2.5,5.0,7.5,10.0]), b)
    return(10.0 ** (a+b))

s_dof_param = "{:.5f}".format(round_power(s_dof_param)).rstrip("0")
print("="*80)
print("Recommended --dofCorrection parameter: {}".format(s_dof_param))
print("="*80)

###############################################################################
