# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# pfms.py
"""
API for interfacing with a `.pfm` directory.
"""

###############################################################################

import os, os.path as op
import re
import numpy, numpy.linalg

import profumo.io as io
import profumo.utilities as utils

###############################################################################
# Class which loads results from a .pfm directory

# https://numpydoc.readthedocs.io/en/latest/format.html
# https://numpydoc.readthedocs.io/en/latest/example.html

class PFMs:
    """
    Python interface with the `.pfm` directories output by PROFUMO.
    
    Attributes
    ----------
    directory : str
        Path to the `.pfm` directory.
    M : int
        Number of PFMs inferred.
    subjects : list
        List of all subject IDs i.e. ['subject1', 'subject2', ...].
    S : int
        Number of subjects analysed.
    runs : dict
        A dictionary of all runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
    R : int
        Total number of runs analysed.
    self.config
        All the options from the run of PROFUMO.
    spatial_model
    time_course_model
    temporal_covariance_model
        Access to the specific forms of the various models.
    mask_filename : str, None
        Path to the mask used to extract the data (or `None` if no mask used).
    """
    
    ###########################################################################
    ###########################################################################
    
    def __init__(self, directory):
        """
        Initialises the PFMs class.
        
        Parameters
        ----------
        directory : str
            Path to the `.pfm` directory.
        """
        
        print("Initialising PFM results...")
        
        def check_dir(dir):
            if not op.isdir(dir):
                raise NotADirectoryError(
                    "Cannot initialise PFMs, no directory found!\n"
                    + "  Directory: {d}".format(d=dir))
            return dir
        
        # Sanitise main directory
        self.directory = check_dir( op.realpath(op.expanduser(directory)) )
        print("PFM directory: {d}".format(d=self.directory))
        
        # Store main results location too
        self.model_directory = op.join(self.directory, 'FinalModel')
        
        # Load the config file
        print("Parsing config file...")
        config_filename = op.join(self.directory, 'Configuration.json')
        self.config = io.load_json(config_filename)
        
        # Save some key config params as local vars
        self.M = self.config['Number of modes']
        self.spatial_model = self.config['Spatial model']
        self.time_course_model = self.config['Temporal model']
        self.temporal_covariance_model = self.config['Covariance model']

        # Record mask location, if used
        if 'Mask file' in self.config:
            mask_name = op.basename(self.config['Mask file'])
            self.mask_filename = op.join(
                    self.directory, 'Preprocessing', mask_name)
            if not op.isfile(self.mask_filename):
                raise FileNotFoundError(
                        "Cannot initialise PFMs, no mask found!\n"
                        + "  Missing file: {f}".format(f=self.mask_filename))
        else:
            self.mask_filename = None
        
        # Load the data locations
        print("Parsing data locations file...")
        locations_filename = op.join(self.directory, 'DataLocations.json')
        data_locations = io.load_json(locations_filename)
        
        # Work out what subjects we have
        print("Generating subject lists...")
        self.subjects = list(data_locations.keys())
        self.S = len(self.subjects)
        print("{s:d} subjects identified, verifying directories..."
              .format(s=self.S))
        # And their directories
        self.subject_directories = {}
        for subject in self.subjects:
            self.subject_directories[subject] = \
                check_dir(op.join(self.model_directory, 'Subjects', subject))
        
        # And the same for the runs
        print("Generating run lists...")
        self.runs = {}
        for subject,runs in data_locations.items():
            self.runs[subject] = list(runs.keys())
        self.R = sum(len(runs) for runs in self.runs.values())
        print("{r:d} runs identified, verifying directories..."
              .format(r=self.R))
        # Including directories
        self.run_directories = {}
        for subject,runs in self.runs.items():
            run_dirs = {}; subject_dir = self.subject_directories[subject]
            for run in runs:
                run_dirs[run] = check_dir(op.join(subject_dir, 'Runs', run))
            self.run_directories[subject] = run_dirs
        
        print("Done.")
        print()
        return
    
    ###########################################################################
    
    def _check_subject(self, subject):
        if not isinstance(subject, str):
            raise TypeError("Subject IDs must be strings!")
        
        if subject not in self.subjects:
            raise ValueError(
                    "Subject '{}' not found in PFM directory"
                    .format(subject))
        
        return True
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
    def _check_run(self, subject, run):
        self._check_subject(subject)
        
        if not isinstance(run, str):
            raise TypeError("Run IDs must be strings!")
        
        if run not in self.runs[subject]:
            raise ValueError(
                    "Run '{}' not found for subject '{}' in PFM directory"
                    .format(run, subject))
        
        return True
    
    ###########################################################################
    ###########################################################################
    
    def load_group_map_means(self, modes=None, signs=None, *, verbose=True):
        """
        Loads the group-level spatial map means from the signal component.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        group_map_means : ndarray
            An [n_voxels, n_modes] matrix.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading group map means...")
        
        # Work out where means are saved
        fixed_params = self.config.get('Fixed group parameters', {})
        if 'Spatial means file' in fixed_params:
            means_filename = op.join(self.directory, 'GroupMapMeans.hdf5')
        # Modes
        elif self.spatial_model == 'Modes':
            means_filename = op.join(self.model_directory, 'GroupSpatialModel',
                                     'SignalMeans.post', 'Means.hdf5')
        # Parcels
        elif self.spatial_model == 'Parcellation':
            means_filename = op.join(self.model_directory, 'GroupSpatialModel',
                                     'Means.post', 'Means.hdf5')
        # Otherwise, ???
        else:
            raise RuntimeError(
                "No group map means available for this spatial model.")
        
        # Load!
        means = io.load_hdf5(means_filename)
        
        # Take a subset of modes, if requested
        if modes is not None:
            means = means[:,modes]
        
        # Flip signs, if requested
        if signs is not None:
            means *= signs
        
        if verbose:
            print("Done.")
            print()
        return means
    
    ###########################################################################
    
    def load_group_map_precisions(self, modes=None, *, verbose=True):
        """
        Loads the group-level spatial map precisions from the signal component.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        
        Returns
        -------
        group_map_precisions : ndarray
            An [n_voxels, n_modes] matrix.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading group map precisions...")
        
        # Work out where precisions are saved
        fixed_params = self.config.get('Fixed group parameters', {})
        if 'Spatial precisions file' in fixed_params:
            prec_filename = op.join(self.directory, 'GroupMapPrecisions.hdf5')
        # Modes
        elif self.spatial_model == 'Modes':
            prec_filename = op.join(self.model_directory, 'GroupSpatialModel',
                                    'SignalPrecisions.post', 'Means.hdf5')
        # Parcels
        elif self.spatial_model == 'Parcellation':
            prec_filename = op.join(self.model_directory, 'GroupSpatialModel',
                                    'Precisions.post', 'Means.hdf5')
        # Otherwise, ???
        else:
            raise RuntimeError(
                "No group map precisions available for this spatial model.")
        
        # Load!
        precisions = io.load_hdf5(prec_filename)
        
        # Take a subset of modes, if requested
        if modes is not None:
            precisions = precisions[:,modes]
        
        if verbose:
            print("Done.")
            print()
        return precisions
    
    ###########################################################################
    
    def load_group_map_noise_precisions(self, *, verbose=True):
        """
        Loads the group-level spatial map precisions from the noise component.
        
        Returns
        -------
        group_map_noise_precisions : ndarray
            An [n_voxels] vector.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading group map noise precisions...")
        
        # Only Modes
        if self.spatial_model == 'Modes':
            prec_filename = op.join(self.model_directory, 'GroupSpatialModel',
                                    'NoisePrecisions.post', 'Means.hdf5')
        else:
            raise RuntimeError(
                "No group map noise precisions available for this spatial model.")
        
        # Load!
        precisions = io.load_hdf5(prec_filename)
        
        if verbose:
            print("Done.")
            print()
        return precisions
    
    ###########################################################################
    
    def load_group_map_memberships(
            self, modes=None, *, normalise=False, verbose=True):
        """
        Loads the group-level spatial map signal probabilities.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        
        Returns
        -------
        group_map_memberships : ndarray
            An [n_voxels, n_modes] matrix.
        
        Other Parameters
        ----------------
        normalise: bool, optional
            If True will rescale the whole matrix such that max=1.0 and
            min=0.0. May help visualisation.
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading group map memberships...")
        
        # Load the memberships
        fixed_params = self.config.get('Fixed group parameters', {})
        if 'Spatial memberships file' in fixed_params:
            memb_filename = self.directory / 'GroupMapMemberships.hdf5'
            memberships = io.load_hdf5(memb_filename)
        # Modes
        elif self.spatial_model == 'Modes':
            memb_filename = op.join(
                self.model_directory, 'GroupSpatialModel', 'Memberships.post',
                'Class_2', 'Probabilities.hdf5')
            memberships = io.load_hdf5(memb_filename)
        # Parcels
        elif self.spatial_model == 'Parcellation':
            memb_dir = op.join(
                self.model_directory, 'GroupSpatialModel', 'Memberships.post')
            memb_filenames = sorted(utils.safe_glob(
                memb_dir, op.join('Class_*', 'Probabilities.hdf5')))
            if len(memb_filenames) != self.M:
                raise FileNotFoundError("Unable to locate the full set of"
                                        + " group spatial memberships.")
            memberships = []
            for memb_filename in memb_filenames:
                memberships.append(io.load_hdf5(memb_filename))
            memberships = numpy.concatenate(memberships, axis=1)
        # Otherwise, ???
        else:
            raise RuntimeError(
                "No group map memberships available for this spatial model.")
        
        # Normalise the probabilities, if requested
        # Expands range so values are between 0.0 and 1.0
        # Can improve visualisation if priors are v. strong
        if normalise and (self.spatial_model == 'Modes'):
            memberships -= numpy.min(memberships)
            memberships /= numpy.max(memberships)
        
        # Take a subset of modes, if requested
        if modes is not None:
            memberships = memberships[:,modes]
        
        if verbose:
            print("Done.")
            print()
        return memberships
    
    ###########################################################################
    
    def load_group_maps(
            self, modes=None, signs=None, *, normalise=False, verbose=True):
        """
        Loads the group-level spatial maps.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        group_maps : ndarray
            An [n_voxels, n_modes] matrix.
        
        Other Parameters
        ----------------
        normalise: bool, optional
            If True will rescale the memberships. Explained in more detail in
            load_group_map_memberships().
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
           print("Loading group maps...")
        
        memberships = self.load_group_map_memberships(
                modes, normalise=normalise, verbose=False)
        # Put together to form maps
        if self.spatial_model == 'Modes':
            means = self.load_group_map_means(modes, signs, verbose=False)
            maps = means * memberships
        elif self.spatial_model == 'Parcellation':
            maps = memberships
        # Otherwise, ???
        else:
            raise RuntimeError(
                "Unrecognised spatial model: {m}"
                .format(m=self.spatial_model))
        
        if verbose:
           print("Done.")
           print()
        return maps
    
    ###########################################################################
    
    def load_subject_maps(
            self, subjects=None, modes=None, signs=None,
            *, clean_maps=True, normalisation='POSTERIOR', verbose=True):
        """
        Loads the subject-specific spatial maps.
        
        Parameters
        ----------
        subjects : list, optional
            A list of subject IDs i.e. ['subject1', 'subject2', ...].
            Default: all subjects.
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        subject_maps : dict
            A dictionary of [n_voxels, n_modes] matrices, using the subject IDs
            as keys i.e. {'subject1': maps; ...}.
        
        Other Parameters
        ----------------
        clean_maps : bool, optional
            Return only the signal (i.e. noise free) part of the posterior.
        normalisation : {'POSTERIOR', 'RAW_DATA', 'PSC'}, optional
            POSTERIOR: Leave 'as is' - that is, the raw posteriors.
            RAW_DATA: Undo internal data normalisation, such that maps could be
                applied to the unadjusted input data.
            PERCENT_SIGNAL_CHANGE: Performs the conversion based on the
                time courses, amplitudes, data normalisations and data means.
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading subject maps...")
        
        if subjects is None:
            subjects = self.subjects
        
        subject_maps = {}
        for s,subject in enumerate(subjects):
            self._check_subject(subject)
            if verbose:
                print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            # Load posterior params and make maps
            if self.spatial_model == 'Modes':
                means = io.load_hdf5(op.join(
                    self.subject_directories[subject], 'SpatialMaps.post',
                    'Signal', 'Means.hdf5'))
                probs = io.load_hdf5(op.join(
                    self.subject_directories[subject], 'SpatialMaps.post',
                    'Signal', 'MembershipProbabilities.hdf5'))
                # Put together
                maps = means * probs
                # Add noise model if requested
                if not clean_maps:
                    noise_means = io.load_hdf5(op.join(
                        self.subject_directories[subject], 'SpatialMaps.post',
                        'Noise', 'Means.hdf5'))
                    noise_scalings = io.load_hdf5(op.join(
                        self.subject_directories[subject], 'SpatialMaps.post',
                        'Noise', 'Scalings.hdf5'))
                    noise_means = noise_means * noise_scalings
                    # And add to maps
                    maps += noise_means * (1.0 - probs)
            
            # Parcellation model
            elif self.spatial_model == 'Parcellation':
                means = io.load_hdf5(op.join(
                    self.subject_directories[subject], 'SpatialMaps.post',
                    'Means.hdf5'))
                probs = io.load_hdf5(op.join(
                    self.subject_directories[subject], 'SpatialMaps.post',
                    'MembershipProbabilities.hdf5'))
                # Put together
                maps = probs
            
            # Otherwise, ???
            else:
                raise RuntimeError("Unrecognised spatial model: {m}"
                                 .format(m=self.spatial_model))
            
            # Take a subset of modes, if requested
            if modes is not None:
                maps = maps[:,modes]
            
            # Flip signs, if requested
            if signs is not None:
                maps *= signs
            
            # Renormalise if we are using modes. If we have a parcellation,
            # the maps are probabilities so don't need rescaling.
            if self.spatial_model == 'Modes':
                maps = self._renormalise_subject_maps(
                        subject, maps, modes, normalisation)
            
            # Record in map structure
            subject_maps[subject] = maps
        
        if verbose:
            print("Done.")
            print()
        return subject_maps
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
    def _renormalise_subject_maps(self, subject, maps, modes, normalisation):
        self._check_subject(subject)
        normalisation = normalisation.upper()
        
        # Load any necessary parameters
        runs = {subject: self.runs[subject]}
        if normalisation == 'RAW_DATA':
            data_normalisations = self.load_data_normalisations(
                    runs, verbose=False)
        elif normalisation == 'PERCENT_SIGNAL_CHANGE':
            data_means = self.load_data_means(runs, verbose=False)
            data_normalisations = self.load_data_normalisations(
                    runs, verbose=False)
            amplitudes = self.load_run_amplitudes(
                    runs, modes, verbose=False)
            time_courses = self.load_run_time_courses(
                runs, modes, clean_time_courses=True, verbose=False)
        
        # Eliminate small values - useful before division
        # N.B. This should only be used when we know the values are positive!
        def remove_zeros(X):
            X[X < 1.0e-7 * numpy.max(X)] = 1.0
            return X
        
        # Apply normalisations
        # Should technically do these to the means only, but as the memberships
        # are just an elementwise multiplication we can do it in either order,
        # and this way stops us having to repeat it for the noise means.
        if normalisation == 'POSTERIOR':
            pass
        
        elif normalisation == 'RAW_DATA':
            # Put the maps back into the original data space
            # Do on a run-by-run basis. Note that this ends up being
            # slightly different to simply applying the average
            # normalisation because of the division.
            run_maps = numpy.zeros(maps.shape)
            for run in runs[subject]:
                run_data_norm = data_normalisations[subject][run]
                run_data_norm = remove_zeros(run_data_norm)
                run_maps += maps / run_data_norm[:, numpy.newaxis]
            # And then just return the mean over runs
            maps = run_maps / len(runs[subject])
        
        elif normalisation == 'PERCENT_SIGNAL_CHANGE':
            # Calculate PSC for each run
            psc = numpy.zeros(maps.shape)
            for run in runs[subject]:
                run_psc = numpy.copy(maps)
                # Put the maps back in the raw data space
                run_data_norm = data_normalisations[subject][run]
                run_data_norm = remove_zeros(run_data_norm)
                run_psc /= run_data_norm[:, numpy.newaxis]
                # Weight maps by signal strength
                run_psc *= (amplitudes[subject][run]
                            * numpy.std(time_courses[subject][run], axis=1))
                # And express as a percentage of the baseline
                run_data_mean = data_means[subject][run]
                run_data_mean = remove_zeros(run_data_mean)
                run_psc /= run_data_mean[:, numpy.newaxis]
                run_psc *= 100.0
                # Store
                psc += run_psc
            # And then just return the mean PSC over runs
            maps = psc / len(runs[subject])
        
        else:
            raise ValueError(
                "Normalisation must be 'POSTERIOR', 'RAW_DATA' or"
                + " 'PERCENT_SIGNAL_CHANGE'!")
        
        return maps
    
    ###########################################################################
    ###########################################################################
    
    def load_run_amplitudes(self, runs=None, modes=None, *, verbose=True):
        """
        Loads the run-specific amplitudes.
        
        Parameters
        ----------
        runs : dict, optional
            A dictionary of runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
            Default: all runs.
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        
        Returns
        -------
        run_amplitudes : dict
            A nested dictionary of [n_modes] vectors, using the subject and run
            IDs as keys i.e.
            {'subject1': {'run1': amplitudes; ...}; ...}.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading amplitudes...")
        
        if runs is None:
            runs = self.runs
        
        amplitudes = {}
        for s,subject in enumerate(runs):
            self._check_subject(subject)
            if verbose:
                print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            # Loop over runs, loading time courses
            subject_amplitudes = {}
            for run in runs[subject]:
                self._check_run(subject, run)
                run_directory = self.run_directories[subject][run]
                run_amplitudes = io.load_hdf5(op.join(
                    run_directory, 'ComponentWeightings.post', 'Means.hdf5'))
                run_amplitudes = numpy.squeeze(run_amplitudes)
                # Take a subset of modes, if requested
                if modes is not None:
                    run_amplitudes = run_amplitudes[modes]
                subject_amplitudes[run] = run_amplitudes
            
            # Record in full structure
            amplitudes[subject] = subject_amplitudes
        
        if verbose:
            print("Done.")
            print()
        return amplitudes
    
    ###########################################################################
    
    def load_group_amplitude_precmat(self, modes=None, *, verbose=True):
        """
        Loads the group-level, between-mode amplitude precision matrix.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        
        Returns
        -------
        group_amplitude_precmat : ndarray
            An [n_modes, n_modes] precision matrix.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading group-level precision matrix on amplitudes...")
        
        precmat = io.load_hdf5(op.join(
            self.model_directory, 'GroupWeightModel',
            'PrecisionMatrix.post', 'Mean.hdf5'))
        
        # Take a subset of modes, if requested
        if modes is not None:
            precmat = precmat[modes,:][:,modes]
        
        if verbose:
            print("Done.")
            print()
        return precmat
    
    ###########################################################################
    ###########################################################################
    
    def load_run_time_courses(self, runs=None, modes=None, signs=None,
                              *, clean_time_courses=True, verbose=True):
        """
        Loads the run-level time courses.
        
        Parameters
        ----------
        runs : dict, optional
            A dictionary of runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
            Default: all runs.
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        run_time_courses : dict
            A nested dictionary of [n_modes, n_time_points] matrices, using the
            subject and run IDs as keys i.e.
            {'subject1': {'run1': time_courses; ...}; ...}.
        
        Other Parameters
        ----------------
        clean_time_courses : bool, optional
            Return only the signal (i.e. noise free) part of the posterior.
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading time courses...")
        
        if runs is None:
            runs = self.runs
        
        time_courses = {}
        for s,subject in enumerate(runs):
            if verbose:
                print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            # Loop over runs, loading time courses
            subject_time_courses = {}
            for run in runs[subject]:
                run_directory = self.run_directories[subject][run]
                if clean_time_courses and ('HRF' in self.time_course_model):
                    run_time_courses = io.load_hdf5(op.join(
                        run_directory, 'TimeCourses.post', 'CleanTimeCourses',
                        'Means.hdf5'))
                else:
                    run_time_courses = io.load_hdf5(op.join(
                        run_directory, 'TimeCourses.post', 'Means.hdf5'))
                
                # Take a subset of modes, if requested
                if modes is not None:
                    run_time_courses = run_time_courses[modes,:]
                
                # Flip signs, if requested
                if signs is not None:
                    run_time_courses *= signs[:, numpy.newaxis]
                
                subject_time_courses[run] = run_time_courses
            
            # Record in full structure
            time_courses[subject] = subject_time_courses
        
        if verbose:
            print("Done.")
            print()
        return time_courses
    
    ###########################################################################
    ###########################################################################
    
    def load_group_time_course_precmats(
            self, modes=None, signs=None, *, verbose=True):
        """
        Loads the group-level, between-mode temporal precision matrices.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        group_time_course_precmats : ndarray / dict
            An [n_modes, n_modes] precision matrix for `Run`, `Subject` model
            types. Otherwise, for the `Condition` model a dictionary
            thereof using the condition labels as keys.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading group-level temporal precision matrices...")
        
        model_type = self.temporal_covariance_model
        model_directory  =  op.join(
            self.model_directory, 'GroupTemporalModel', 'PrecisionMatrixModel')
        
        # Get the locations of the saved matrices
        locations = {}
        if model_type == 'Subject' or model_type == 'Run':
            locations['Group'] = op.join(model_directory, 'Mean.hdf5')
        
        elif model_type == 'Condition':
            # For run-level models we also need to work out what the models
            # are called. These are just the unique run names.
            unique_runs = \
                {run for subject in self.runs for run in self.runs[subject]}
            # Each model saved in its own named directory
            for run in unique_runs:
                locations[run] = op.join(model_directory, run, 'Mean.hdf5')
        
        # Otherwise, ???
        else:
            raise RuntimeError(
                    "Unrecognised temporal precision matrix model: {m}"
                    .format(m=model_type))
        
        # Load prior degrees of freedom
        # We use this to adjust the above (which are priors on the subject/run
        # rate matrices) such that they are in the same space as the subject
        # precmats.
        dof_filename = op.join(
                self.model_directory, 'GroupTemporalModel',
                'PrecisionMatrixModel', 'PriorDegreesOfFreedom.txt')
        with open(dof_filename) as dof_file:
            dof = float( dof_file.readlines()[0].strip() )
        
        # Load posteriors
        precmats = {}
        for model_class, filename in locations.items():
            rate_matrix = io.load_hdf5(filename)
            # Have to invert (Wisharts switch every level in the hierarchy)
            # And then have to account for the scaling
            # i.e. E[subjPrecMat] = a * B^-1 = dof * E[groupPosterior]^-1 (ish)
            precmat = dof * numpy.linalg.inv(rate_matrix)
            
            # Take a subset of modes, if requested
            if modes is not None:
                precmat = precmat[modes,:][:,modes]
            
            # Flip signs, if requested
            if signs is not None:
                precmat *= signs[:,numpy.newaxis]
                precmat *= signs[numpy.newaxis,:]
            
            # And record
            precmats[model_class] = precmat
        
        # If we only have one group precmat, just return it 'as is'
        if model_type == 'Subject' or model_type == 'Run':
            precmats = precmats['Group']
        
        if verbose:
            print("Done.")
            print()
        return precmats
    
    ###########################################################################
    
    def load_run_time_course_precmats(
            self, runs=None, modes=None, signs=None, *, verbose=True):
        """
        Loads the run-level temporal precision matrices.
        
        Parameters
        ----------
        runs : dict, optional
            A dictionary of runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
            Default: all runs.
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        run_time_course_precmats : dict
            A nested dictionary of [n_modes, n_modes] precision matrices,
            using the subject and run IDs as keys i.e.
            {'subject1': {'run1': precmat; ...}; ...}.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading run-level temporal precision matrices...")
        
        model_type = self.temporal_covariance_model
        
        if runs is None:
            runs = self.runs
        
        precmats = {}
        for s,subject in enumerate(runs):
            if verbose:
                print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            # Loop over runs, loading precmats
            run_precmats = {}
            for run in runs[subject]:
                
                # Get locations
                if model_type == 'Subject':
                    filename = op.join(
                        self.subject_directories[subject],
                        'TemporalPrecisionMatrix.post', 'Mean.hdf5')
                
                elif model_type == 'Run' or model_type == 'Condition':
                    filename = op.join(
                        self.run_directories[subject][run],
                        'TemporalPrecisionMatrix.post', 'Mean.hdf5')
                
                # Otherwise, ???
                else:
                    raise RuntimeError(
                        "Unrecognised temporal precision matrix model: {m}"
                        .format(m=model_type))
                
                # And load
                precmat = io.load_hdf5(filename)
                
                # Take a subset of modes, if requested
                if modes is not None:
                    precmat = precmat[modes,:][:,modes]
                
                # Flip signs, if requested
                if signs is not None:
                    precmat *= signs[:,numpy.newaxis]
                    precmat *= signs[numpy.newaxis,:]
                
                # Record run
                run_precmats[run] = precmat
            
            # And then subject
            precmats[subject] = run_precmats
        
        if verbose:
            print("Done.")
            print()
        return precmats
    
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
    def load_subject_time_course_precmats(
        self, subjects=None, modes=None, signs=None, *, verbose=True):
        """
        Wrapper function for load_run_time_course_precmats(), but that just
        returns the single precision matrix per subject for the `Subject`
        model.

        See load_run_time_course_precmats() for all the arguments.
        
        Parameters
        ----------
        subjects : list, optional
            A list of subject IDs i.e. ['subject1', 'subject2', ...].
            Default: all subjects.
        """
        
        if subjects is None:
            subjects = self.subjects
        
        # Load run-specific precmats
        runs = {subject: self.runs[subject] for subject in subjects}
        precmats = self.load_run_time_course_precmats(
                runs, modes, signs, verbose=False)
        
        # If subject level, remove the run dictionaries
        if self.temporal_covariance_model == 'Subject':
            for subject in subjects:
                # Take average over runs
                # Note they are all identical, so could just pick one too
                precmats[subject] = numpy.mean(
                    numpy.stack(precmats[subject].values(), axis=-1), axis=-1)
        
        return precmats
    
    ###########################################################################
    ###########################################################################
    
    def load_data_normalisations(self, runs=None, *, verbose=True):
        """
        Loads the run-level data normalisation vectors.
        
        These were generated during postprocessing, and allow the data to be
        rescaled such that it matches what PROFUMO analysed:
        `profumo_data = normalisation * (data - data_means)`
        
        Parameters
        ----------
        runs : dict, optional
            A dictionary of runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
            Default: all runs.
        
        Returns
        -------
        data_normalisations : dict
            A nested dictionary of [n_voxels] vectors, using the subject and
            run IDs as keys i.e.
            {'subject1': {'run1': data_normalisations; ...}; ...}.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading data normalisation parameters...")
        
        if self.config['Data normalisation'] is False:
            raise RuntimeError(
                    "Cannot load data normalisations - this option"
                    + " was disabled when running PROFUMO!")
        
        if runs is None:
            runs = self.runs
        
        normalisations = {}
        for s,subject in enumerate(runs):
            if verbose:
                print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            # Now load
            subject_normalisations = {}
            for run in runs[subject]:
                norm_filename = op.join(self.directory, 'Preprocessing',
                                        subject, run + '_Normalisation.hdf5')
                run_norm = numpy.squeeze( io.load_hdf5(norm_filename) )
                subject_normalisations[run] = run_norm
            
            # Record in full structure
            normalisations[subject] = subject_normalisations
        
        if verbose:
            print("Done.")
            print()
        return normalisations
    
    ###########################################################################
    
    def load_data_means(self, runs=None, *, verbose=True):
        """
        Loads the run-level voxelwise data means.
        
        Parameters
        ----------
        runs : dict, optional
            A dictionary of runs i.e. {'subject1': ['run1', 'run2', ...]; ...}.
            Default: all runs.
        
        Returns
        -------
        data_means : dict
            A nested dictionary of [n_voxels] vectors, using the subject and
            run IDs as keys i.e.
            {'subject1': {'run1': data_means; ...}; ...}.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading data means...")
        
        if runs is None:
            runs = self.runs
        
        means = {}
        for s,subject in enumerate(runs):
            if verbose:
                print("Subject {s:d}: {subj}".format(s=s+1, subj=subject))
            
            # Now load
            subject_means = {}
            for run in runs[subject]:
                mean_filename = op.join(self.directory, 'Preprocessing',
                                        subject, run + '_Means.hdf5')
                run_mean = numpy.squeeze( io.load_hdf5(mean_filename) )
                subject_means[run] = run_mean
            
            # Record in full structure
            means[subject] = subject_means
        
        if verbose:
            print("Done.")
            print()
        return means
    
    ###########################################################################
    ###########################################################################
    
    def load_spatial_basis(self, *, verbose=True):
        """
        Loads the spatial singular vectors calculated by the group-level SVD
        during initialisation.
        
        Returns
        -------
        spatial_basis : ndarray
            An [n_voxels, n_modes] matrix.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading spatial basis...")
        
        if 'Fixed group parameters' in self.config:
            raise RuntimeError(
                "Cannot load spatial basis - this is not calculated when"
                + " running PROFUMO with fixed group parameters!")
        
        spatial_basis = io.load_hdf5(op.join(
            self.directory, 'Preprocessing', 'SpatialBasis.hdf5'))
        
        if verbose:
            print("Done.")
            print()
        return spatial_basis
    
    ###########################################################################
    
    def load_initial_maps(self, modes=None, signs=None, *, verbose=True):
        """
        Loads the spatial maps used to initialise the full PFM model.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        initial_maps : ndarray
            An [n_voxels, n_modes] matrix.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading spatial maps used for initialisation...")
        
        initial_maps = io.load_hdf5(op.join(
            self.directory, 'Preprocessing', 'InitialMaps.hdf5'))
        
        # Take a subset of modes, if requested
        if modes is not None:
            initial_maps = initial_maps[:,modes]
        
        # Flip signs, if requested
        if signs is not None:
            initial_maps *= signs
        
        if verbose:
            print("Done.")
            print()
        return initial_maps
    
    ###########################################################################
    
    def load_intermediate_maps(self, modes=None, signs=None, *, verbose=True):
        """
        Loads the snapshots of the group maps saved as PROFUMO ran.
        
        Parameters
        ----------
        modes : list, optional
            Subset of modes to return.
            Default: all modes.
        signs : list, optional
            Sign flips to apply to the (subset of) modes.
            Default: no sign flipping performed.
        
        Returns
        -------
        intermediate_maps : dict
            A nested dictionary of [n_voxels, n_modes] matrices, using the
            model and iteration numbers as keys i.e.
            {'Model1': {0: maps; 1: maps; ...}; ...}.
        
        Other Parameters
        ----------------
        verbose: bool, optional
            If True prints progress to stdout.
        """
        
        if verbose:
            print("Loading intermediate spatial maps...")
        
        # Get list of model directories
        model_directories = sorted(utils.safe_glob(
            op.join(self.directory, 'Intermediates'), 'Model*'))
        
        # Loop over these, loading the intermediates within
        intermediates = {}
        for model_directory in model_directories:
            model = op.basename(model_directory)
            if verbose:
                print("Loading intermediates from {}...".format(model))
            
            # Get a list of the intermediates
            filenames = sorted(
                utils.safe_glob(model_directory, 'GroupMaps_*.hdf5'))
            
            # Loop through, loading all of these
            model_intermediates = {}
            for n,filename in enumerate(filenames):
                # Extract iter number from file name
                iteration = re.search('GroupMaps_([0-9]*).hdf5',
                                      op.basename(filename))
                iteration = int( iteration.groups()[0] )
                
                if verbose:
                    print("Intermediate {n:d}: iteration {iter:d}"
                          .format(n=n+1, iter=iteration))
                
                # Load intermediate
                intermediate = io.load_hdf5(filename)
                
                # Take a subset of modes, if requested
                if modes is not None:
                    intermediate = intermediate[:,modes]
                
                # Flip signs, if requested
                if signs is not None:
                    intermediate *= signs
                
                # Record
                model_intermediates[iteration] = intermediate
            
            # And store
            intermediates[model] = model_intermediates
            if verbose:
                print()
        
        if verbose:
            print("Done.")
            print()
        return intermediates
    
    ###########################################################################
    ###########################################################################

###############################################################################
