==6450== Memcheck, a memory error detector
==6450== Copyright (C) 2002-2015, and GNU GPL'd, by Julian Seward et al.
==6450== Using Valgrind-3.12.0 and LibVEX; rerun with -h for copyright info
==6450== Command: /home/fs0/rezvanh/bin/PROFUMO_SNLdev /vols/Scratch/rezvanh/dummy_subjects/DataLocations10.json 10 /vols/Scratch/rezvanh/dummy_subjects_nifti_6 --bigData --useHRF 0.72 --hrfFile /home/fs0/rezvanh/PROFUMO/Scripts/DefaultHRF.phrf --multiStartIterations 1 -k 20 -d 0.05
==6450== Parent PID: 75414
==6450== 
==6450== 
==6450== HEAP SUMMARY:
==6450==     in use at exit: 18,040 bytes in 25 blocks
==6450==   total heap usage: 173,646 allocs, 173,621 frees, 507,611,178 bytes allocated
==6450== 
==6450== 12,160 bytes in 20 blocks are possibly lost in loss record 6 of 6
==6450==    at 0x4A06EDD: calloc (vg_replace_malloc.c:710)
==6450==    by 0x3456611D02: _dl_allocate_tls (in /lib64/ld-2.12.so)
==6450==    by 0x34572072CC: pthread_create@@GLIBC_2.2.5 (in /lib64/libpthread-2.12.so)
==6450==    by 0x368500D2BD: ??? (in /usr/lib64/libgomp.so.1.0.0)
==6450==    by 0x3685009AE7: GOMP_parallel (in /usr/lib64/libgomp.so.1.0.0)
==6450==    by 0x5E42DC: PROFUMO::DataLoader<PROFUMO::DataTypes::LowRankData>::DataLoader(std::string, std::string, unsigned int, std::unique_ptr<PROFUMO::DataTransformer<PROFUMO::DataTypes::LowRankData>, std::default_delete<PROFUMO::DataTransformer<PROFUMO::DataTypes::LowRankData> > >, std::unique_ptr<PROFUMO::SaveTransformedData<PROFUMO::DataTypes::LowRankData>, std::default_delete<PROFUMO::SaveTransformedData<PROFUMO::DataTypes::LowRankData> > >, std::unique_ptr<PROFUMO::LoadTransformedBatch<PROFUMO::DataTypes::LowRankData>, std::default_delete<PROFUMO::LoadTransformedBatch<PROFUMO::DataTypes::LowRankData> > >, std::shared_ptr<arma::Col<unsigned long long> const>, bool, bool, bool, std::string, std::vector<std::string, std::allocator<std::string> >) (DataLoader.h:177)
==6450==    by 0x5DE865: PROFUMO::DataLoaders::LowRankDataLoader::LowRankDataLoader(std::string, std::string, unsigned int, unsigned int, std::shared_ptr<arma::Col<unsigned long long> const>, bool, bool, bool, std::string, std::vector<std::string, std::allocator<std::string> >) (LowRankDataLoader.c++:195)
==6450==    by 0x4766CA: void __gnu_cxx::new_allocator<PROFUMO::DataLoaders::LowRankDataLoader>::construct<PROFUMO::DataLoaders::LowRankDataLoader, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool, std::string const&>(PROFUMO::DataLoaders::LowRankDataLoader*, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool&&, std::string const&) (new_allocator.h:120)
==6450==    by 0x46DAE8: void std::allocator_traits<std::allocator<PROFUMO::DataLoaders::LowRankDataLoader> >::construct<PROFUMO::DataLoaders::LowRankDataLoader, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool, std::string const&>(std::allocator<PROFUMO::DataLoaders::LowRankDataLoader>&, PROFUMO::DataLoaders::LowRankDataLoader*, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool&&, std::string const&) (alloc_traits.h:475)
==6450==    by 0x46314E: std::_Sp_counted_ptr_inplace<PROFUMO::DataLoaders::LowRankDataLoader, std::allocator<PROFUMO::DataLoaders::LowRankDataLoader>, (__gnu_cxx::_Lock_policy)2>::_Sp_counted_ptr_inplace<std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool, std::string const&>(std::allocator<PROFUMO::DataLoaders::LowRankDataLoader>, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool&&, std::string const&) (shared_ptr_base.h:520)
==6450==    by 0x457B6D: std::__shared_count<(__gnu_cxx::_Lock_policy)2>::__shared_count<PROFUMO::DataLoaders::LowRankDataLoader, std::allocator<PROFUMO::DataLoaders::LowRankDataLoader>, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool, std::string const&>(std::_Sp_make_shared_tag, PROFUMO::DataLoaders::LowRankDataLoader*, std::allocator<PROFUMO::DataLoaders::LowRankDataLoader> const&, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool&&, std::string const&) (shared_ptr_base.h:615)
==6450==    by 0x44F21D: std::__shared_ptr<PROFUMO::DataLoaders::LowRankDataLoader, (__gnu_cxx::_Lock_policy)2>::__shared_ptr<std::allocator<PROFUMO::DataLoaders::LowRankDataLoader>, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool, std::string const&>(std::_Sp_make_shared_tag, std::allocator<PROFUMO::DataLoaders::LowRankDataLoader> const&, std::string const&, std::string const&, unsigned int const&, unsigned int const&, std::shared_ptr<arma::Col<unsigned long long> const> const&, bool const&, bool const&, bool&&, std::string const&) (shared_ptr_base.h:1100)
==6450== 
==6450== LEAK SUMMARY:
==6450==    definitely lost: 0 bytes in 0 blocks
==6450==    indirectly lost: 0 bytes in 0 blocks
==6450==      possibly lost: 12,160 bytes in 20 blocks
==6450==    still reachable: 5,880 bytes in 5 blocks
==6450==         suppressed: 0 bytes in 0 blocks
==6450== Reachable blocks (those to which a pointer was found) are not shown.
==6450== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==6450== 
==6450== For counts of detected and suppressed errors, rerun with: -v
==6450== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 4 from 4)
