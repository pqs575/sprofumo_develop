# PROFUMO

A method for inferring for [PRObabilistic FUnctional
MOdes] from fMRI data.
Developed by the FMRIB Analysis Group at the [Wellcome Centre for Integrative
Neuroimaging](https://www.win.ox.ac.uk/), University of Oxford, to integrate
with FSL.

Original PROFUMO paper: https://doi.org/10.1016/j.neuroimage.2015.01.013

Second PROFUMO paper: https://doi.org/10.1016/j.neuroimage.2020.117226

Stochastic PROFUMO paper: https://doi.org/10.1016/j.neuroimage.2021.118513 

This repository includes the latest changes in stochastic PROFUMO (sPROFUMO), to be integrated within [PROFUMO](https://git.fmrib.ox.ac.uk/samh/profumo) and [FSL](https://fsl.fmrib.ox.ac.uk).
----------

Version stochastic beta; 2021-11-06

!! Note that this repository is actively under development. If you are considering using it, please get in touch with Rezvan Farahibozorg:
rezvan.farahibozorg@ndcn.ox.ac.uk

[Repository](https://git.fmrib.ox.ac.uk/rezvanh/sprofumo_develop)
&mdash;
[License](LICENSE)
&mdash;
[Contributors](CONTRIBUTORS.md)


Running the code after installation
------------

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/lib

export OMP_NUM_THREADS=20

N_MODES=50
TR=0.72 #e.g. for HCP

$HOME/bin/sPROFUMO_develop /YOUR_PATH_TO/DataLocations.json $N_MODES \
/YOUR_PATH_TO/sprofumo_output_dir.pfm \
--bigData --stochasticBeta 0.6 --stochasticBatchSize 50 --stochInitBatchUpdates 10 --groupIteration 4000 --subjectIteration 2.5 \
-m /YOUR_PATH_TO/brain_mask.nii.gz \
--useHRF $TR --hrfFile /YOUR_PATH_TO/sprofumo_develop/Scripts/DefaultHRF.phrf --multiStartIterations 10 -d 0.075 > /YOUR_PATH_TO/sprofumo_log.txt

#run $HOME/bin/sPROFUMO_develop --help for info on different arguments. 

#--bigData flag enables sPROFUMO usage 
