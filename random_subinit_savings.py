#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 02:10:02 2019

@author: rezvanh
"""

if (firstBatch){
            runModel.model = groupWeights_;
        } else {
            arma::fvec previousRWM;
            arma::fvec previousRNM;
            // Make a Run model
            const std::string dirWeight = bigDataSubjDir + "Runs" +"/" +runInformation.runID + "/"+"ComponentWeightings.post"+"/"+ "Means.hdf5";
        
            std::ifstream fileWeight(dirWeight.c_str());                       
            
        	// Check if object is valid
            if (fileWeight.good()) {
                previousRWM = Utilities::loadDataMatrix(dirWeight);
            
            } else {
                throw std::runtime_error("Run's weight model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID+", Run: "+runInformation.runID); 
            } 
            
            
            MFModels::H::P2C HEbasis;
            HEbasis.h = previousRWM;
            HEbasis.hht = HEbasis.h * HEbasis.h.t();// check if this is good enough? + 0.1 * previousSWM.n_rows * arma::eye<arma::fmat>(previousSWM.n_cols, previousSWM.n_cols);
            std::shared_ptr<MFModels::H_VBPosterior> Hbasis = std::make_shared<VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>>(HEbasis);

            SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
            runModel.model = Hbasis;
        
        }
            
            
// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "CategorisedRunPrecisionMatrices.h"

#include <sys/stat.h>
#include <fstream>

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::CategorisedRunPrecisionMatrices(const VBModules::PrecisionMatrices::Wishart::Parameters groupPrior, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior)
: groupPrior_(groupPrior), runPrior_(runPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation, const bool firstBatch, const std::string bigDataSubjDir)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // No subject level precision matrix
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Either make, or assign from pool of, run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        
        if (firstBatch){
            // See if we have this in the pool already, and if not make it
            if (groupPrecisions_.count(runID) == 0) {
                groupPrecisions_.emplace(runID, std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior_));
            }
            // Make the run level model
            runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecisions_.at(runID).get(), runPrior_);
        } else {
            const std::string dirMean = bigDataSubjDir + "TemporalPrecisionMatrix.post" +"/" + "Means.hdf5"; //membership
            std::ifstream fileMean(dirMean.c_str());
            
            if (fileMean.good()) {
                arma::fmat subjectPrecMat = Utilities::loadDataMatrix(dirMean);
                std::cout << subjectPrecMat.n_cols <<std::endl;            
                std::shared_ptr<Modules::MatrixMean_VBPosterior> sPMM_Means; //std::shared_ptr<Modules::MatrixMean_Parent>  
                Modules::MatrixMeans::P2C Emeans;
                Emeans.X = subjectPrecMat; 
                sPMM_Means = std::make_shared<VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>>(Emeans);                
                runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(sPMM_Means.get(), runPrior_);
                        
            } else {
                throw std::runtime_error("Run's temporal precision matrix model wasn't saved the first time it was picked in a batch! Subject: " + subjectInformation.subjectID+", Run: "+runInformation.runID); 
            }
        
        
        }
        runModel.isInternalModel = true;
        // And record
        subjectModel.runLevel.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Use the mean precision matrix
    // Initialise with precision matrix from first category
    auto gPM = groupPrecisions_.cbegin();
    arma::fmat X = gPM->second->getExpectations().X;
    // Add other models if necessary
    for (++gPM; gPM != groupPrecisions_.cend(); ++gPM) {
        X += gPM->second->getExpectations().X;
    }
    // And normalise
    X /= groupPrecisions_.size();
    
    // Transform hyperprior on rate matrix to precision matrix
    return runPrior_.a * linalg::inv_sympd(X);
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::updateModel()
{
    for (auto& groupPrecision : groupPrecisions_) {
        groupPrecision.second->update();
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posteriors
    for (const auto& groupPrecision : groupPrecisions_) {
        RunID runID = groupPrecision.first;
        const std::string runDir = directory + runID + "/";
        mkdir( runDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupPrecision.second->save(runDir);
    }
    
    // And save prior (needed to calculate run expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    std::ofstream file;
    file.open( fileName.c_str() );
    file << runPrior_.a << std::endl;
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

float PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getModelKL() const
{
    float KL = 0.0;
    
    for (const auto& groupPrecision : groupPrecisions_) {
        KL += groupPrecision.second->getKL();
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
