/*
  Matthew Webster (WIN@FMRIB)
  Copyright (C) 2018 University of Oxford  */
/*  CCOPYRIGHT  */
#if !defined(__newnifti_h)
#define __newnifti_h

#include <string>
#include <vector>
#include "nifti2.h"
#include "legacyFunctions.h"
#include "znzlib/znzlib.h"

struct NiftiException : public std::exception
{
   std::string errorMessage;
   NiftiException(const std::string& error) : errorMessage(error) {}
   ~NiftiException() throw() {}
   const char* what() const throw() { return errorMessage.c_str(); }
};

class NiftiExtension
{
public:
int    esize ;
int    ecode ;
std::vector<char> edata ;
size_t extensionSize() const { return( sizeof(esize) + sizeof(ecode) + edata.size() ); }
};

class NiftiHeader
{
private:
void initialise(void);
public:
NiftiHeader();
NiftiHeader(std::vector<std::string> XMLreport);
//Common Header parameters
int sizeof_hdr;
int64_t vox_offset;
std::string magic;
short datatype;
int bitsPerVoxel;
std::vector<int64_t> dim;
std::vector<double> pixdim;
int intentCode;
double intent_p1;
double intent_p2;
double intent_p3;
std::string intentName;
double sclSlope;
double sclInter;
double cal_max;
double cal_min;
double sliceDuration;
double toffset;
int64_t sliceStart;
int64_t sliceEnd;
std::string description;
std::string auxillaryFile;
int qformCode;
int sformCode;
double qB;
double qC;
double qD;
double qX;
double qY;
double qZ;
std::vector<double> sX;
std::vector<double> sY;
std::vector<double> sZ;
int sliceCode;
int units;
char sliceOrdering;
//Useful extras
std::string fileType() const;
std::string niftiOrientationString( const int orientation ) const;
std::string niftiSliceString() const;
std::string niftiIntentString() const;
std::string niftiTransformString(const int transform) const;
std::string unitsString(const int units) const;
std::string datatypeString() const;
std::string originalOrder() const;
void sanitise();
char freqDim() const { return sliceOrdering & 3; }
char phaseDim() const { return (sliceOrdering >> 2) & 3; }
char sliceDim() const { return (sliceOrdering >> 4) & 3; }
char niftiVersion() const { return NIFTI2_VERSION(*this); }
bool isAnalyze() const { return ( (NIFTI2_VERSION(*this) == 1) && (NIFTI_VERSION(*this) == 0) ); }
bool wasWrongEndian;
bool singleFile() const { return NIFTI_ONEFILE(*this); }
size_t nElements() const { size_t elements(dim[1]); for (int dims=2;dims<=dim[0];dims++) elements*=dim[dims]; return elements; }
int bpvOfDatatype(void);
mat44 getQForm() const;
void setQForm(const mat44& qForm);
std::string qFormName() const { return niftiTransformString(qformCode);}
mat44 getSForm() const;
void setSForm(const mat44& sForm);
std::string sFormName() const { return niftiTransformString(qformCode);}
int leftHanded() const { return( (pixdim[0] < 0.0) ? -1.0 : 1.0 ) ; }
void setNiftiVersion( const char niftiVersion, const bool isSingleFile );
void report() const;
 void resetNonNiftiFields();

private:
};



class NiftiIO
{
public:
  bool debug;
  NiftiHeader loadImage( std::string filename, char*& buffer, std::vector<NiftiExtension>& extensions, bool allocateBuffer=true);
  NiftiHeader loadHeader(const std::string filename);
  NiftiHeader loadExtensions(const std::string filename, std::vector<NiftiExtension>& extensions);
  NiftiHeader loadImageROI( std::string filename, char*& buffer, std::vector<NiftiExtension>& extensions, size_t& bufferElements, int64_t xmin=-1, int64_t xmax=-1, int64_t ymin=-1, int64_t ymax=-1, int64_t zmin=-1, int64_t zmax=-1, int64_t tmin=-1, int64_t tmax=-1, int64_t d5min=-1, int64_t d5max=-1, int64_t d6min=-1, int64_t d6max=-1, int64_t d7min=-1, int64_t d7max=-1);
  void saveImage(const std::string filename, const char* buffer, const std::vector<NiftiExtension>& extensions, const NiftiHeader header, const bool useCompression=true, const bool sanitise=true);
  NiftiIO(void);
  template<class T>
  void reportHeader(const T& header);
private:
  znzFile file;
  void openImage( const std::string filename, const bool reading, const bool useCompression=true );
  void readRawBytes( void *buffer, size_t length );
  void writeRawBytes( const void *buffer, size_t length );
  void readVersion( NiftiHeader& header );
  void readData(const NiftiHeader& header,void* buffer);
  void readExtensions(const NiftiHeader& header, std::vector<NiftiExtension>& extensions );
  void writeData(const NiftiHeader& header,const void* buffer);
  NiftiHeader readHeader();
  void readNifti1Header(NiftiHeader& header);
  void readNifti2Header(NiftiHeader& header);
  template<class T>
    void storeCommonNiftiHeader( const T& rawNiftiHeader, NiftiHeader& header );
  template<class T>
    void byteSwap(T& rawNiftiHeader);
  void byteSwap(const size_t elementLength, void* buffer,const unsigned long nElements=1);
  void byteSwapLegacy(nifti_1_header& rawNiftiHeader);
  void writeHeader(const NiftiHeader& header);
  void writeAnalyzeHeader(const NiftiHeader& header);
  void writeNifti1Header(const NiftiHeader& header);
  void writeNifti2Header(const NiftiHeader& header);
  template<class T>
    void retrieveCommonImageHeader( T& rawNiftiHeader, const NiftiHeader& header );
  template<class T>
    void retrieveCommonNiftiHeader( T& rawNiftiHeader, const NiftiHeader& header );
  void writeExtensions(const NiftiHeader& header, const std::vector<NiftiExtension>& extensions );
  void reportLegacyHeader(const nifti_1_header& header);
};
#endif
