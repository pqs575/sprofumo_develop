#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

using namespace std; 

#include "NewNifti.h"

template <class T>
void print4(T* buffer)
{
  cerr << (double)buffer[0] << " " << (double)buffer[1] <<  " " << (double)buffer[2] << " " << (double)buffer[3] << endl;
}

int main(int argc,char *argv[])
{
  NiftiIO Reader;
  char* buffer;
  NiftiHeader header,header2;;
  Reader.debug=true;
  vector<NiftiExtension> extensions;
    header = Reader.loadImageROI(string(argv[1]),buffer,extensions,-1,-1,-1,-1,-1,-1,-1,-1);
    header.report();


  for ( unsigned int ext=0;ext<extensions.size();ext++) {
    cerr << (int)extensions[ext].esize;
    cerr << endl;
  }
  if ( header.datatype == NIFTI_TYPE_UINT8 )
    print4((char *)buffer);
  else if ( header.datatype == NIFTI_TYPE_INT16 )
    print4((short *)buffer);
  else if ( header.datatype == NIFTI_TYPE_INT32 )
    print4((int *)buffer);
  else if ( header.datatype == NIFTI_TYPE_FLOAT32 )
    print4((float *)buffer);
  else if ( header.datatype == NIFTI_TYPE_FLOAT64 )
    print4((double *)buffer);
  return 0;
}


