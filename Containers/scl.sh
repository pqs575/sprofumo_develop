# PROFUMO and sPROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2019, Rezvan Farahibozorg 2021
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# Should be sourced after FSL has been activated (i.e. after `fsl.sh`), as gcc7
# is not a recognised compiler for $FSLMACHTYPE.


# https://stackoverflow.com/a/677212
if command -v scl_source >/dev/null 2>&1; then
    source scl_source enable devtoolset-8
    # scl enable sets PYTHONPATH, so unset it and let conda manage everything
    unset PYTHONPATH
fi
