ifeq ($(PREFIX),)
    PREFIX := $(shell pwd)/dist
endif

#
# 'make'
# 'make all'
# 'make PROFUMO'
# 'make Test'
# 'make install'
# 'make uninstall'
# 'make clean'
#

#################################################################################
# Based on:
# http://www.cs.swarthmore.edu/~newhall/unixhelp/howto_makefiles.html
# http://stackoverflow.com/questions/5273374/make-file-with-source-subdirectories

# Convention used here:
# ${HOME} for imported environment variables
# $(OBJS) for variables defined in this file
#################################################################################



#################################################################################
# Options

# Define any libraries to link into executable
LDFLAGS += -larmadillo -lhdf5 -lNewNifti -lopenblas  -lznz -lm -lz  -lstdc++fs

# Machine specific changes to these
CXXFLAGS += -I. -ISource -fopenmp -fPIE

# -ffast-math
# Define the C++ source files
SRCS := $(shell find Source -name '*.c++')
OBJS := $(SRCS:.c++=.o)

# Get a timestamp
TIMESTAMP := $(shell date +'%Y.%m.%d-%H:%M:%S')

#################################################################################
# Build rules

.PHONY: all install uninstall clean FORCE

all: PROFUMO Test

# Make only applies implicit C++ rules
# to files ending in .cc or .cpp, not
# to files ending in .c++.
%.o: %.c++
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

# Rules to link all the object files together to make the executables
PROFUMO: $(OBJS) PROFUMO.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(OBJS) PROFUMO.o -o PROFUMO $(LDFLAGS)

Test: $(OBJS) Test.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(OBJS) Test.o -o Test $(LDFLAGS)

install: PROFUMO
	cp PROFUMO $(PREFIX)/bin/sPROFUMO_develop_$(TIMESTAMP)
	ln -sf sPROFUMO_develop_$(TIMESTAMP) $(PREFIX)/bin/sPROFUMO_develop

uninstall:
	rm -f $(prefix)/bin/sPROFUMO_develop*

clean:
	rm -f $(OBJS) PROFUMO PROFUMO.o Test Test.o

# Forcing rebuild of BuildTime.c++ means the build timestamp is correct
Source/Utilities/BuildTime.o: FORCE
