// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A generic VBModule that just holds a copy of expectations.

#ifndef VB_MODULES_COPYPARENT_H
#define VB_MODULES_COPYPARENT_H

#include <string>

#include "Module.h"
#include "Posterior.h"

namespace PROFUMO
{
    namespace VBModules
    {
        ////////////////////////////////////////////////////////////////////////
        
        template <class C2P, class P2C>
        class CopyParent :
            public Module_Parent<C2P, P2C>,
            protected CachedModule<P2C>
        {
        public:
            // Construct from a supplied set of expectations
            CopyParent(const P2C expectations);
            
            
        };
    
        ////////////////////////////////////////////////////////////////////////
        // Constructor

        template <class C2P, class P2C>
        CopyParent<C2P,P2C>::CopyParent(const P2C expectations)
        {
            CachedModule<P2C>::expectations_ = expectations;
            return;
        }
    
        
    
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
