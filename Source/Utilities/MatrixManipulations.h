// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some useful techniques for manipulating matrices - including using VB matrix  
// factorisation approaches.

#ifndef UTILITIES_MATRIX_MANIPULATIONS
#define UTILITIES_MATRIX_MANIPULATIONS

#include <armadillo>

#include "DataTypes.h"

namespace PROFUMO
{
    namespace Utilities
    {
        namespace MatrixManipulations
        {
            ////////////////////////////////////////////////////////////////////
            
            // Randomly rotate the columns of a matrix
            void randomiseColumns(arma::fmat& X);
            
            // Make all column means positive
            void flipColumnMeans(arma::fmat& X);
            
            // Make all column skews positive
            void flipColumnSkews(arma::fmat& X);
            
            // Variant of the above based on the third moment around zero
            // (i.e. rather than the mean)
            void flipColumnSigns(arma::fmat& X);
            
            ////////////////////////////////////////////////////////////////////
            
            // Cosine similarity between matrix columns
            // https://en.wikipedia.org/wiki/Cosine_similarity
            arma::fmat calculateCosineSimilarity(const arma::fmat& X);
            arma::fmat calculateCosineSimilarity(const arma::fmat& X, const arma::fmat& Y);
            
            // Pair the columns of two matrices based on their cosine similarity
            struct Pairing
            {
            public:
                arma::uvec inds1, inds2;
                arma::fvec scores;
            };
            
            Pairing pairMatrixColumns(const arma::fmat& X, const arma::fmat& Y);
            
            ////////////////////////////////////////////////////////////////////
            
            // Does a quick VB factorisation of a matrix
            // Finds a set of M components that best explain the data, 
            // according to the model D = P * H * A, and returns P (a column 
            // basis)
            
            enum class FactorisationType {
                MODES,
                PARCELS
            };
            
            struct Decomposition
            {
            public:
                arma::fmat P; // Maps
                arma::fvec h; // Weights
                arma::fmat A; // Time courses
                arma::fmat Alpha; // Temporal Precmat
                float F;      // Free energy
            };
            
            Decomposition calculateVBMatrixFactorisation(const DataTypes::FullRankData data, const unsigned int rank, const FactorisationType factorisationType, const arma::fmat& initialisation, const unsigned int iterations);
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
