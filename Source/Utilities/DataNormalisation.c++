// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DataNormalisation.h"

#include <cmath>
#include <stdexcept>
#include <string>

namespace DN = PROFUMO::Utilities::DataNormalisation;

////////////////////////////////////////////////////////////////////////////////
// Demean
////////////////////////////////////////////////////////////////////////////////

// Removes the data mean
float DN::demeanGlobally(arma::fmat& D)
{
    const float mean = arma::mean( arma::vectorise(D) );
    
    D -= mean;
    
    return mean;
}

////////////////////////////////////////////////////////////////////////////////

// Removes the data mean from each row
arma::fvec DN::demeanRows(arma::fmat& D)
{
    const arma::fvec means = arma::mean(D,1);
    
    D.each_col() -= means;
    
    return means;
}

////////////////////////////////////////////////////////////////////////////////

// Removes the data mean from each column
arma::frowvec DN::demeanColumns(arma::fmat& D)
{
    const arma::frowvec means = arma::mean(D,0);
    
    D.each_row() -= means;
    
    return means;
}

////////////////////////////////////////////////////////////////////////////////
// Generic functions for normalisation
// These all normalise D based on another matrix ref
////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace Utilities
    {
        namespace DataNormalisation
        {
            float normaliseGlobally(arma::fmat& D, const arma::fmat& ref, const float tolerance);
            arma::fvec normaliseRows(arma::fmat& D, const arma::fmat& ref, const float tolerance);
            void normaliseRowsAB(arma::fmat& D, const arma::fmat& ref, const float tolerance, const float a, const float b);
            arma::frowvec normaliseColumns(arma::fmat& D, const arma::fmat& ref, const float tolerance);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

// Rescales data to unit variance
float DN::normaliseGlobally(arma::fmat& D, const arma::fmat& ref, const float tolerance)
{
    // Compute std from ref
    float stddev = arma::stddev( arma::vectorise(ref) );
    // Prevent division by 0
    if (stddev < tolerance) {
        stddev = 1.0;
    }
    if (!std::isfinite(stddev)) {
        std::stringstream errorMessage;
        errorMessage << "DataNormalisation: Unable to safely compute standard deviation of matrix.\n";
        errorMessage << "  Value returned: " << stddev << "\n";
        throw std::runtime_error( errorMessage.str() );
    }
    
    // Normalise data
    const float normalisation = 1.0 / stddev;
    D *= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data to unit variance
arma::fvec DN::normaliseRows(arma::fmat& D, const arma::fmat& ref, const float tolerance)
{
    // Compute std from ref
    arma::fvec stddev = arma::stddev(ref, 0, 1);
    // Prevent division by 0
    stddev.elem( arma::find(stddev < tolerance) ).ones();
    // In pathological cases, the variance calculation can underflow giving a 
    // negative variance (which then gets mangled by std::sqrt() before here)
    stddev.elem( arma::find_nonfinite(stddev) ).ones();
    
    // Normalise data
    const arma::fvec normalisation = 1.0 / stddev;
    D.each_col() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data between a (min: default= -1), b (max: default= 1)
void DN::normaliseRowsAB(arma::fmat& D, const arma::fmat& ref, const float tolerance, const float a, const float b)
{
    // Compute std from ref
    arma::fvec refMin = arma::min(ref, 1);
    arma::fvec refMax = arma::max(ref, 1);
    arma::fvec refDiff = refMax-refMin;
    // Prevent division by 0
    refDiff.elem( arma::find(refDiff < tolerance) ).ones();
    
    // Normalise data
    D.each_col() -= refMin;
    const arma::fvec normalisation = 1.0 / refDiff;
    D.each_col() %=normalisation;
    D *= b-a;
    D += a;
    
    return;
    
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each column of the data to unit variance
arma::frowvec DN::normaliseColumns(arma::fmat& D, const arma::fmat& ref, const float tolerance)
{
    // Compute std from ref
    arma::frowvec stddev = arma::stddev(ref, 0, 0);
    // Prevent division by 0
    stddev.elem( arma::find(stddev < tolerance) ).ones();
    stddev.elem( arma::find_nonfinite(stddev) ).ones();
    
    // Normalise data
    const arma::frowvec normalisation = 1.0 / stddev;
    D.each_row() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////
// Overall normalisation
////////////////////////////////////////////////////////////////////////////////

// Rescales data to unit variance
float DN::normaliseGlobally(arma::fmat& D, const float tolerance)
{
    // Straightforward normalisation - the reference is the data!
    const arma::fmat& ref = D;
    return normaliseGlobally(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data to unit variance
arma::fvec DN::normaliseRows(arma::fmat& D, const float tolerance)
{
    // Straightforward normalisation - the reference is the data!
    const arma::fmat& ref = D;
    return normaliseRows(D, ref, tolerance);
}
////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data to unit variance
void DN::normaliseRowsAB(arma::fmat& D, const float tolerance, const float a, const float b)
{
    // Straightforward normalisation - the reference is the data!
    const arma::fmat& ref = D;
    normaliseRowsAB(D, ref, tolerance, a, b);
    return;
}
////////////////////////////////////////////////////////////////////////////////

// Rescales each column of the data to unit variance
arma::frowvec DN::normaliseColumns(arma::fmat& D, const float tolerance)
{
    // Straightforward normalisation - the reference is the data!
    const arma::fmat& ref = D;
    return normaliseColumns(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////
// Signal subspace normalisation
////////////////////////////////////////////////////////////////////////////////

// Rescales data to unit variance
float DN::normaliseSignalSubspaceGlobally(arma::fmat& D, const unsigned int M, const float tolerance)
{
    // Compute SVD
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace captured by the SVD
    const arma::fmat ref = svd.U * arma::diagmat(svd.s) * svd.V.t();
    
    // And normalise!
    return normaliseGlobally(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data to unit variance
arma::fvec DN::normaliseSignalSubspaceRows(arma::fmat& D, const unsigned int M, const float tolerance)
{
    // Compute SVD
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace captured by the SVD
    const arma::fmat ref = svd.U * arma::diagmat(svd.s) * svd.V.t();
    
    // And normalise!
    return normaliseRows(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each column of the data to unit variance
arma::frowvec DN::normaliseSignalSubspaceColumns(arma::fmat& D, const unsigned int M, const float tolerance)
{
    // Compute SVD
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace captured by the SVD
    const arma::fmat ref = svd.U * arma::diagmat(svd.s) * svd.V.t();
    
    // And normalise!
    return normaliseColumns(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////
// Noise subspace normalisation
////////////////////////////////////////////////////////////////////////////////

// Rescales data to unit variance
float DN::normaliseNoiseSubspaceGlobally(arma::fmat& D, const unsigned int M, const float tolerance)
{
    // Compute SVD
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace outside of SVD
    const arma::fmat ref = D - svd.U * arma::diagmat(svd.s) * svd.V.t();
    
    // And normalise!
    return normaliseGlobally(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data to unit variance
arma::fvec DN::normaliseNoiseSubspaceRows(arma::fmat& D, const unsigned int M, const float tolerance)
{
    // Compute SVD
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace outside of SVD
    const arma::fmat ref = D - svd.U * arma::diagmat(svd.s) * svd.V.t();
    
    // And normalise!
    return normaliseRows(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each column of the data to unit variance
arma::frowvec DN::normaliseNoiseSubspaceColumns(arma::fmat& D, const unsigned int M, const float tolerance)
{
    // Compute SVD
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace outside of SVD
    const arma::fmat ref = D - svd.U * arma::diagmat(svd.s) * svd.V.t();
    
    // And normalise!
    return normaliseColumns(D, ref, tolerance);
}

////////////////////////////////////////////////////////////////////////////////
