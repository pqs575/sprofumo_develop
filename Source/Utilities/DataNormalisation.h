// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Provides several different ways of normalising a data matrix (in place)

#ifndef UTILITIES_DATA_NORMALISATION_H
#define UTILITIES_DATA_NORMALISATION_H

#include <armadillo>
#include <limits>

#include "Utilities/RandomSVD.h"

namespace PROFUMO
{
    namespace Utilities
    {
        namespace DataNormalisation
        {
            // Any entities with standard deviation less than tol will not 
            // be normalised. Defaults to machine precision.
            const float tol = std::numeric_limits<float>::epsilon();
            
            
            // Sets mean(D) = 0
            // Returns the mean that has been removed
            float demeanGlobally(arma::fmat& D);
            arma::fvec demeanRows(arma::fmat& D);
            arma::frowvec demeanColumns(arma::fmat& D);
            
            
            // Sets std(D) = 1
            // Returns the multiplicative normalisation that has been applied
            // i.e. update was D_new = norm * D_old
            float normaliseGlobally(arma::fmat& D, const float tolerance = tol);
            arma::fvec normaliseRows(arma::fmat& D, const float tolerance = tol);
            void normaliseRowsAB(arma::fmat& D, const float tolerance = tol, const float a = -1, const float b = 1);
            arma::frowvec normaliseColumns(arma::fmat& D, const float tolerance = tol);
            
            // Normalise by the subspace of the top M principal components
            // i.e. this tries to set std(signal) = 1
            float normaliseSignalSubspaceGlobally(arma::fmat& D, const unsigned int M, const float tolerance = tol);
            arma::fvec normaliseSignalSubspaceRows(arma::fmat& D, const unsigned int M, const float tolerance = tol);
            arma::frowvec normaliseSignalSubspaceColumns(arma::fmat& D, const unsigned int M, const float tolerance = tol);
            
            // Normalise by the subspace of the (N-M) lowest principal components
            // i.e. this tries to set std(noise) = 1
            float normaliseNoiseSubspaceGlobally(arma::fmat& D, const unsigned int M, const float tolerance = tol);
            arma::fvec normaliseNoiseSubspaceRows(arma::fmat& D, const unsigned int M, const float tolerance = tol);
            arma::frowvec normaliseNoiseSubspaceColumns(arma::fmat& D, const unsigned int M, const float tolerance = tol);
            
        }
    }
}
#endif
