// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some functions for reading/writing data from/to file.

#ifndef UTILITIES_DATA_IO_H
#define UTILITIES_DATA_IO_H

#include <armadillo>
#include <string>

#include "NewNifti/NewNifti.h"

namespace PROFUMO
{
    namespace Utilities
    {
    
        // Loads a data matrix from file
        arma::fmat loadDataMatrix(const std::string fileName);
        
        // Checks to see whether or not a file name has a given extension
        bool hasExtension(const std::string fileName, const std::string extension);
        
    }
}
#endif
