// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "BuildTime.h"

////////////////////////////////////////////////////////////////////////////////

std::string PROFUMO::Utilities::getBuildTime()
{
    return std::string(__TIME__);
}

////////////////////////////////////////////////////////////////////////////////

std::string PROFUMO::Utilities::getBuildDate()
{
    return std::string(__DATE__);
}

////////////////////////////////////////////////////////////////////////////////
