// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */
#include <stdexcept>
#include "DataTypes.h"

////////////////////////////////////////////////////////////////////////////////
// FullRankData
///////////////////////////////////////////////////////////////////////////////

PROFUMO::DataTypes::FullRankData::FullRankData(const std::shared_ptr<const arma::fmat> D)
: D(D), V(D->n_rows), T(D->n_cols)
{
    return;
}

PROFUMO::DataTypes::FullRankData::FullRankData(const arma::fmat& D)
: D(std::make_shared<const arma::fmat>(D)), V(D.n_rows), T(D.n_cols)
{
    return;
}


////////////////////////////////////////////////////////////////////////////////
// LowRankData
////////////////////////////////////////////////////////////////////////////////

PROFUMO::DataTypes::LowRankData::LowRankData(const std::shared_ptr<const arma::fmat> Pd, const std::shared_ptr<const arma::fmat> Ad, const unsigned int N, const float TrDtD)
: Pd(Pd), Ad(Ad), N(N), V(Pd->n_rows), T(Ad->n_cols), TrDtD(TrDtD)
{
    // Check sizes are OK
    if ( (Pd->n_cols != N) || (Ad->n_rows != N) ) {
        // All hell breaks loose
        throw std::runtime_error("Inconsistent matrix sizes initialising DataTypes::LowRankData!");
    }
    
    return;
}

///////////////////////////////////////////////////////////////////////////////


