// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Implementation of DataLoader for FullRankData.

#ifndef DATA_LOADERS_FULL_RANK_DATA_LOADER_H
#define DATA_LOADERS_FULL_RANK_DATA_LOADER_H

#include "DataLoader.h"
#include "DataTypes.h"

namespace PROFUMO
{
    namespace DataLoaders
    {
        ///////////////////////////////////////////////////////////////////////
        
        class FullRankDataTransformer :
            public DataTransformer<DataTypes::FullRankData>
        {
        public:
            // Converts a set of data matrices into FullRankData, and saves them in the case of bigdata
            virtual std::map<RunID, DataTypes::FullRankData> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const SubjectID subjectID, const bool BD = false, const std::string bigDataDir = "") const;
            //virtual std::map<RunID, DataTypes::FullRankData> transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const; //, const SubjectID subjectID, const std::string bigDataDir = ""

        };
        
        ///////////////////////////////////////////////////////////////////////
        
        class FullRankSaveTransformedData :
            public SaveTransformedData<DataTypes::FullRankData>
        {
        public:
            // Converts a set of data matrices into FullRankData, and saves them in the case of bigdata
            //virtual std::map<RunID, DataTypes::FullRankData> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const SubjectID subjectID, const bool BD = false, const std::string bigDataDir = "") const;
            virtual void saveTransformedSubjectData(const std::map<RunID, DataTypes::FullRankData> transformedSubjectData, const SubjectID subjectID, const bool BD = false, const std::string bigDataDir = "") const; //, const SubjectID subjectID, const std::string bigDataDir = ""
            
        

        };
        ///////////////////////////////////////////////////////////////////////
        
        class FullRankDataTransformedBatch :
            public LoadTransformedBatch<DataTypes::FullRankData>
        {
        public:
            // Converts a set of data matrices into FullRankData, and saves them in the case of bigdata
            //virtual std::map<RunID, DataTypes::FullRankData> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices, const SubjectID subjectID, const bool BD = false, const std::string bigDataDir = "") const;
            virtual std::map<RunID, DataTypes::FullRankData> transformBatchSubjectData(const std::map<RunID, std::string>& runInfo) const; //, const SubjectID subjectID, const std::string bigDataDir = ""

        };
        
        ////////////////////////////////////////////////////////////////////////
        
        class FullRankDataLoader :
            public DataLoader<DataTypes::FullRankData>
        {
        public:
            // Constructor: just loads all the data
            FullRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, std::shared_ptr<const arma::uvec> maskInds = nullptr, const bool normaliseData = true, const bool BD = false, const bool batchLoader = false, const std::string bigDataDir = "", const std::vector<std::string> subNamesList = {""});
            
            // Computes the spatial basis for all the data
            virtual arma::fmat computeSpatialBasis(unsigned int K, const bool MIGP, const std::string bigDataDir="") const;
        };
    
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
