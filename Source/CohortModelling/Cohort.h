// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a Cohort: a class that contains the group level parameters and a set 
// of subjects.

#ifndef COHORT_MODELLING_COHORT_H
#define COHORT_MODELLING_COHORT_H

#include <memory>
#include <vector>
#include <string>
#include <armadillo>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>

#include "SubjectModelling/Run.h"
#include "SubjectModelling/Subject.h"

#include "Utilities/DataIO.h"

#include "CohortModelling/GroupModel.h"

#include "Utilities/Timer.h"

#include "MFModels/P/DGMM.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/A/KroneckerHRF.h"
#include "MFModels/A/AdditiveMultivariateNormal.h"
#include "MFModels/H/MultivariateNormal.h"
#include "MFModels/H/SpikeSlab.h"
#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "Module.h"
#include "Posterior.h"
#include "VBModules/Constant.h"

//NOTE! GroupSpatialModel takes a copy of all GroupModel<SubjectModelling::Subject::SpatialModel> methods and params + a method to get group map
namespace PROFUMO
{
    namespace CohortModelling
    {
            
        class Cohort
        {
        public:
            // Constructor templated by data type
            template<class D>
            Cohort(const std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  groupSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   groupWeightModel, 
                   std::shared_ptr<GroupTemporalModel> groupTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    groupNoiseModel,
                   const float dofCorrectionFactor=1.0);
                  
            template<class D>
            void resetSubjects(const std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  initialSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   initialWeightModel, 
                   std::shared_ptr<GroupTemporalModel> initialTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    initialNoiseModel,
                   const float dofCorrectionFactor=1.0,
                   std::string bigDataDirBatch = {""});
                               
            // Update the models!
            void update(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0, const bool updateGroup = true);
            void updateSpatialModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0, const bool updateGroup = true);
            void updateWeightModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0, const bool updateGroup = true);
            void updateTemporalModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0, const bool updateGroup = true);
            void updateNoiseModel(const bool bigData = false, const float posteriorRho = 1.0, const int subjectNum = 0, const bool updateGroup = true);
            
            void signFlipSpatialModel(const bool flipGroup = true, const bool flipSubjects = true);
            // Functions to dynamically switch models
            void setSpatialModel(std::shared_ptr<GroupSpatialModel>   newGroupSpatialModel);
            void setWeightModel(std::shared_ptr<GroupWeightModel>     newGroupWeightModel);
            void setTemporalModel(std::shared_ptr<GroupTemporalModel> newGroupTemporalModel);
            void setNoiseModel(std::shared_ptr<GroupNoiseModel>       newGroupNoiseModel);
            
            // Save everything to file
            void save(const std::string directory, const bool saveSubjects = true, const bool saveGroup = true, const bool replaceSubjects = true) const;
            void clear(const bool groupMapSignFlip = true);
            
            // Get F
            arma::frowvec getFreeEnergy(const bool bigData = false, const int subjectNum = 0, const float posteriorRho = 1.0, const std::string bigDataDirBatch = {""});
            
            // Get current group parameters
            arma::fmat getGroupMaps() const;
            arma::fmat getGroupPrecisionMatrix() const;
            std::vector<std::string> getSubjectIDs() const;
            arma::fmat getAllSubjectsFreeEnergy();

            
            // Whether to normalise time course variance
            void setTimeCourseNormalisation(const bool normalisation);
            
            struct preinitialisedSubjectOptions {
            public:
                const bool firstBatch = true;
                const bool firstSubject = true;
                const std::string bigDataSubjDir = {""};
                
            };
            
            std::vector<std::string>   allSubjectInformations_;
            
        private:
            // List of subjects
            std::vector<std::unique_ptr<SubjectModelling::Subject>> subjects_;
            unsigned int S_;
            //arma::frowvec Fsub = arma::zeros<arma::frowvec>(9); 
            arma::fmat groupMeans;
            
            // Store for group models
            std::shared_ptr<GroupSpatialModel>  initialSpatialModel_;
            std::shared_ptr<GroupSpatialModel>  groupSpatialModel_;
            std::shared_ptr<GroupWeightModel>   groupWeightModel_;
            std::shared_ptr<GroupTemporalModel> groupTemporalModel_;
            std::shared_ptr<GroupNoiseModel>    groupNoiseModel_;
            
            
        };
        
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

// Constructor templated by data type
template<class D>
PROFUMO::CohortModelling::Cohort::Cohort(
                   std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  groupSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   groupWeightModel, 
                   std::shared_ptr<GroupTemporalModel> groupTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    groupNoiseModel,
                   const float dofCorrectionFactor)
: groupSpatialModel_(groupSpatialModel), groupWeightModel_(groupWeightModel), groupTemporalModel_(groupTemporalModel), groupNoiseModel_(groupNoiseModel)
{
    S_ = subjectInformation.size();
    // Make the subjects!
    for (const SubjectInformation subject : subjectInformation) {
        
        // Make the models
        auto sSM = groupSpatialModel_->getSubjectModel(subject);
        auto sWM = groupWeightModel_->getSubjectModel(subject);
        auto sTM = groupTemporalModel_->getSubjectModel(subject);
        auto sNM = groupNoiseModel_->getSubjectModel(subject);
        
        // And then the subject
        allSubjectInformations_.push_back(subject.subjectID);
        subjects_.push_back( std::make_unique<SubjectModelling::Subject>(subject, subjectData.at(subject.subjectID), sSM, sWM, sTM, sNM, dofCorrectionFactor) );
        
    }
    
    return;
}
// reset subjects templated by data type- only for big data
template<class D>
void PROFUMO::CohortModelling::Cohort::resetSubjects(
                   std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  initialSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   initialWeightModel, 
                   std::shared_ptr<GroupTemporalModel> initialTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    initialNoiseModel,
                   const float dofCorrectionFactor,
                   std::string bigDataDirBatch)
{
    //for (auto thisSubject : subjects_)
    //{
    //    delete thisSubject;
    //}
    //subjects_.clear(); 
    S_ = subjectInformation.size();
    
    //groupSpatialModel_->resetSubjects();
    //groupWeightModel_->resetSubjects();
    //groupTemporalModel_->resetSubjects();
    //groupNoiseModel_->resetSubjects();
    //std::cout<<"batch cohort!"<<std::endl;
    for (const SubjectInformation subject : subjectInformation) {
        // define path here
        // if spatialmodel::modes else spatialmodel::parcellation
        const std::string dirName = bigDataDirBatch + "Subjects" + "/" + subject.subjectID + "/"; //+ "/" + "SpatialMaps.post" + "/" + "Signal" + "/" + "Means.hdf5";
        std::cout<< dirName <<std::endl;
        DIR* thisDir = opendir(dirName.c_str());
        
        //SubjectModelling::Subject::Model<VBPosterior<MFModels::P::C2P, MFModels::P::P2C>> sSM;
        //SubjectModelling::Subject::WeightModel sWM;
        //SubjectModelling::Subject::TemporalModel sTM;
        //SubjectModelling::Subject::NoiseModel sNM;
        //std::shared_ptr<MFModels::P_VBPosterior> Pbasis;
        // Make the models
        if (thisDir) { 
            std::cout<<"not first time!"<<std::endl;          
            // Spatial Model
            auto sSM = groupSpatialModel_->getSubjectModel(subject, {false,false}, bigDataDirBatch);
            auto sWM = groupWeightModel_->getSubjectModel(subject, {false,false}, bigDataDirBatch);
            auto sTM = groupTemporalModel_->getSubjectModel(subject, {false,false}, bigDataDirBatch);
            auto sNM = groupNoiseModel_->getSubjectModel(subject, {false,false}, bigDataDirBatch);
                        
            
            // And then the subject
            allSubjectInformations_.push_back(subject.subjectID);
            subjects_.push_back( std::make_unique<SubjectModelling::Subject>(subject, subjectData.at(subject.subjectID), sSM, sWM, sTM, sNM, dofCorrectionFactor) ); 
            closedir(thisDir);
                            
    
        } else {
            std::cout<<"first batch!"<<std::endl;
            auto sSM = groupSpatialModel_->getSubjectModel(subject, {false,true}, bigDataDirBatch);
            auto sWM = groupWeightModel_->getSubjectModel(subject, {false,true}, bigDataDirBatch);
            auto sTM = groupTemporalModel_->getSubjectModel(subject, {false,true}, bigDataDirBatch);
            auto sNM = groupNoiseModel_->getSubjectModel(subject, {false,true}, bigDataDirBatch);
            
            // And then the subject
            allSubjectInformations_.push_back(subject.subjectID);
            subjects_.push_back( std::make_unique<SubjectModelling::Subject>(subject, subjectData.at(subject.subjectID), sSM, sWM, sTM, sNM, dofCorrectionFactor) );  
            
                 
        }
                    
        
    }
    //for (unsigned int s = 0; s < S_; ++s) {
    //    subjects_[s]->signFlipSubjectMap(spatialSigns);
    //    subjects_[s]->signFlipSubjectWeight(spatialSigns);
    //}
    
    return;


}
////////////////////////////////////////////////////////////////////////////////
/*
// Batch initialiser templated by data type
template<class D>
PROFUMO::CohortModelling::Cohort::BatchCohort(
                   const std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  initialSpatialModel,
                   std::shared_ptr<GroupSpatialModel>  groupSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   groupWeightModel, 
                   std::shared_ptr<GroupTemporalModel> groupTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    groupNoiseModel,
                   std::string bigDataDirBatch,
                   const float dofCorrectionFactor)
: S_(subjectInformation.size()), initialSpatialModel_(initialSpatialModel), groupSpatialModel_(groupSpatialModel), groupWeightModel_(groupWeightModel), groupTemporalModel_(groupTemporalModel), groupNoiseModel_(groupNoiseModel)
{
    // Make the subjects!
    for (const SubjectInformation subject : subjectInformation) {
        // define path here
        // if spatialmodel::modes else spatialmodel::parcellation
        const std::string dirName = bigDataDirBatch + "Subjects" + "/" + subject.subjectID; //+ "/" + "SpatialMaps.post" + "/" + "Signal" + "/" + "Means.hdf5";
        // Make the models
        if (opendir(dirName.c_str())) {           
            
            const std::string dirDGMM = dirName + "Signal" + "/"+ "Means.hdf5"; //membership
            std::ifstream fileDGMM(dirDGMM.c_str());
            
            const std::string dirParcellations = dirName + "Means.hdf5"; //membership
            std::ifstream fileParcellations(dirParcellations.c_str());
            
            const std::string dirOtherModels = dirName + "Means.hdf5"; //mean covariance
            std::ifstream fileOtherModels(dirOtherModels.c_str());           
            //const std::string dirRandConst = dirName + "Means.hdf5"; //innerproduct ptp
            //const std::string dirAdditiveGaussian = dirName + "Means.hdf5"; //mean covariance
            //const std::string dirIndependentMixtureModels = dirName + "Means.hdf5"; //innerproduct ptp
            //const std::string dirPottsParcellation = dirName + "Means.hdf5"; //membership 
            
            arma::fmat previousSubjectMap;
        	// Check if object is valid
            if (fileDGMM.good()) {
                arma::fmat subjectSpatialMean = PROFUMO::Utilities::loadDataMatrix(dirDGMM);
                std::cout << subjectSpatialMean.n_cols <<std::end;
                const std::string memProbFile = dirName + "Signal" + "/"+ "MembershipProbabilities.hdf5";
                arma::fmat subjectSpatialMemProb = PROFUMO::Utilities::loadDataMatrix(memProbFile);
                previousSubjectMap = subjectSpatialMean % subjectSpatialMemProb;
                
            } else if (fileParcellations.good()) {
                arma::fmat subjectSpatialMean = PROFUMO::Utilities::loadDataMatrix(dirParcellations);
                std::cout << subjectSpatialMean.n_cols <<std::end;
                const std::string memProbFile = dirName + "MembershipProbabilities.hdf5";
                arma::fmat subjectSpatialMemProb = PROFUMO::Utilities::loadDataMatrix(memProbFile);
                previousSubjectMap = subjectSpatialMean % subjectSpatialMemProb;
                    
            } else if (fileOtherModels.good()) {
                previousSubjectMap = PROFUMO::Utilities::loadDataMatrix(dirOtherModels);
            
            } else {
                throw std::runtime_error("Subject's model wasn't saved the first time it was picked in a batch!" + subject.subjectID); 
            } 
            
            MFModels::P::P2C Ebasis;
            Ebasis.P = previousSubjectMap;
            Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.1 * previousSubjectMap.n_rows * arma::eye<arma::fmat>(previousSubjectMap.n_cols, previousSubjectMap.n_cols);
            std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
            std::shared_ptr<SubjectModelling::Subject::SpatialModel> sSM = std::make_shared<SubjectModelling::Subject::SpatialModel>(Pbasis);
        
        } else {
            auto sSM = initialSpatialModel_->getSubjectModel(subject);
                 
        }
        auto sWM = groupWeightModel_->getSubjectModel(subject);
        auto sTM = groupTemporalModel_->getSubjectModel(subject);
        auto sNM = groupNoiseModel_->getSubjectModel(subject);   
        
        
        // And then the subject
        subjects_.emplace_back( new SubjectModelling::Subject(subject, subjectData.at(subject.subjectID), sSM, sWM, sTM, sNM, dofCorrectionFactor) );
    }
    
    return;
}
*/
////////////////////////////////////////////////////////////////////////////////
#endif
