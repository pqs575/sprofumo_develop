// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015, Rezvan Farahibozorg 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Minimal class structure that allows us to construct graphical models

#ifndef MODULE_H
#define MODULE_H

#include <vector>

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that just defines that a module has to return expectations
    //
    // We can use this to define reciprocal parent/child relationships between
    // different modules, and from there can construct whole graphical models.
    // Obviously, this message passing framework already makes some assumptions
    // about how we are going to solve the model, but they should not be too
    // restrictive. For example, this structure allows modules to represent
    // data, VB posteriors, posteriors inferred via sampling, constants etc
    
    template<class E>
    class Module
    {
    public:
        virtual E getExpectations() const = 0;
        
        virtual ~Module() = default;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that defines a parent in a graphical model. This can
    // have an arbitrary number of children (c.f. multiple observations from a
    // distribution).
    
    // Expectations:
    // C2P: Child -> Parent
    // P2C: Parent -> Child
    
    template<class C2P, class P2C>
    class Module_Parent :
        public virtual Module<P2C>
    {
    public:
        void addChildModule(Module<C2P>* childModule);
        void resetParentModule();
        
    protected:
        typedef std::vector<Module<C2P>*> listType;
        listType childModules_;
    };
    
    
    template<class C2P, class P2C>
    void Module_Parent<C2P, P2C>::addChildModule(Module<C2P>* childModule)
    {
        #pragma omp critical
        {
        childModules_.push_back(childModule);
        }
        
        return;
    }
    
    
    template<class C2P, class P2C>
    void Module_Parent<C2P, P2C>::resetParentModule()
    {        
        childModules_.clear();        
        return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that defines a child in a graphical model. This has one
    // parent of this type, though can have multiple different parents via
    // multiple inheritance (c.f. mean and variance hyperpriors)
    
    // Expectations:
    // C2P: Child -> Parent
    // P2C: Parent -> Child
    
    template<class C2P, class P2C>
    class Module_Child :
        public virtual Module<C2P>
    {
    public:
        Module_Child(Module_Parent<C2P, P2C>* parentModule);
        
        void setParentModule(Module_Parent<C2P, P2C>* parentModule);
        
    protected:
        Module<P2C>* parentModule_;
    };
    
    
    // Has to be constructed with the parent present
    template<class C2P, class P2C>
    Module_Child<C2P, P2C>::Module_Child(Module_Parent<C2P, P2C>* parentModule)
    {
        setParentModule(parentModule);
        
        return;
    }
    
    // Code to set parent distribution (can be changed dynamically if required)
    template<class C2P, class P2C>
    void Module_Child<C2P, P2C>::setParentModule(Module_Parent<C2P, P2C>* parentModule)
    {
        // Record parent
        parentModule_ = parentModule;
        // And set up link
        parentModule->addChildModule(this);
        
        return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that just defines a module where there is an internal
    // cache for the expectations
    
    template<class E>
    class CachedModule :
        public virtual Module<E>
    {
    public:
        E getExpectations() const;
        
    protected:
        E expectations_;
    };
    
    
    template<class E>
    E CachedModule<E>::getExpectations() const
    {
        return expectations_;
    }
    
    ////////////////////////////////////////////////////////////////////////////
}
#endif
