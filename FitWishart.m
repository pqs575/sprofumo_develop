%%

addpath(genpath('~/Documents/Code/MATLAB/'))
prettyFigures();

clear all

WBC='/vols/Data/HCP/workbench/workbench/bin_rh_linux64/wb_command';

%% Load data

BO = ciftiopen('/vols/Data/HCP/Phase2/subjectsE/101107/RESOURCES/rfMRI_REST1_LR_FIX/rfMRI_REST1_LR/rfMRI_REST1_LR_Atlas_hp2000_clean.dtseries.nii', WBC);
D{1} = double(BO.cdata);

BO = ciftiopen('/vols/Data/HCP/Phase2/subjectsE/101107/RESOURCES/rfMRI_REST1_RL_FIX/rfMRI_REST1_RL/rfMRI_REST1_RL_Atlas_hp2000_clean.dtseries.nii', WBC);
D{2} = double(BO.cdata);

BO = ciftiopen('/vols/Data/HCP/Phase2/subjectsE/101107/RESOURCES/rfMRI_REST2_LR_FIX/rfMRI_REST2_LR/rfMRI_REST2_LR_Atlas_hp2000_clean.dtseries.nii', WBC);
D{3} = double(BO.cdata);

BO = ciftiopen('/vols/Data/HCP/Phase2/subjectsE/101107/RESOURCES/rfMRI_REST2_RL_FIX/rfMRI_REST2_RL/rfMRI_REST2_RL_Atlas_hp2000_clean.dtseries.nii', WBC);
D{4} = double(BO.cdata);

clear BO;

[V,T] = size(D{1});

%% De-mean and normalise variance

for i = 1:4
    D{i} = bsxfun(@minus, D{i}, mean(D{i},2));
    D{i} = D{i} / std(D{i}(:));
    D{i} = bsxfun(@times, D{i}, 1./std(D{i}')');
end

max(abs(mean(D{1},2)))
max(std(D{1}'))

%% Do PCA

Dfull = [D{1} D{2} D{3} D{4}];

[A,B,C] = princomp(Dfull, 'econ');

%% Visualise

figure; plot([1 4800], [0 0], '--', 'Color', 0.7*[1 1 1]); 
hold on; plot(C); xlim([1 4800]); ylim(max(C)*[-0.1 1.1]);

figure; TwoColourTufteHist(C)
figure; TwoColourTufteHist(C, 'xlim', [0 2])


%% 

[Uf,Sf,Vf] = svd(Dfull,'econ');

%% Do random low-rank PCA

N = 500;

%%
tic

% Random test matrices
Z = cell(4,1);
for i = 1:4
    Z{i} = randn(T,N);
end

% Approximate spatial basis
Y = zeros(V,N);
for i = 1:4
    Y = Y + D{i} * Z{i};
end
%Power approximation
for q = 1:3
    Y2 = zeros(V,N);
    for i = 1:4
        Y2 = Y2 + D{i} * (D{i}' * Y);
    end
    Y = Y2;
    
    % Make orthonormal
    Y = Y * (Y'*Y)^(-0.5);
    Y = bsxfun(@times, Y, 1./sqrt(sum(Y.^2)));
end

toc

%%
tic

Y = Dfull * randn(4*T,N);
for q = 1:3
    Y = Dfull * (Dfull' * Y);
    
    % Make orthonormal
    Y = Y * (Y'*Y)^(-0.5);
    Y = bsxfun(@times, Y, 1./sqrt(sum(Y.^2)));
end

toc
%%

% SVD of temporal basis
X = zeros(N, 4*T);
for i = 1:4
    X(:,((i-1)*T)+(1:T)) = Y' * D{i};
end

[Ux,Sx,Vx] = svd(X,'econ');


% Combine to get full SVD
[sd, inds] = sort(diag(Sx), 'descend');
Ud = Y * Ux; Ud = Ud(:,inds);
Vd = Vx(:,inds);

figure; plot(diag(Sf)); hold on; plot(sd, 'r')
figure; imagesc(corrcoef([Uf(:,1:N), Ud]), [-1 1]); colorbar; colormap(bluewhitered)

