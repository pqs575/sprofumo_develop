# Module which customises matplotlib

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker

import numpy

import glob
import re
import os

################################################################################
# Import some extra colour maps

# See work by Matteo Niccoli:
# https://mycarta.wordpress.com/color-palettes/

def loadColourMap(name, fileName):
    colourMap = numpy.loadtxt(fileName, delimiter=",")
    
    # Convert csv file to colour map
    # Code from:
    # https://mycarta.wordpress.com/2014/04/25/convert-color-palettes-to-python-matplotlib-colormaps/
    b3=colourMap[:,2] # value of blue at sample n
    b2=colourMap[:,2] # value of blue at sample n
    b1=numpy.linspace(0,1,len(b2)) # position of sample n - ranges from 0 to 1
    
    # setting up columns for list
    g3=colourMap[:,1]
    g2=colourMap[:,1]
    g1=numpy.linspace(0,1,len(g2))
    
    r3=colourMap[:,0]
    r2=colourMap[:,0]
    r1=numpy.linspace(0,1,len(r2))
    
    # creating list
    R=zip(r1,r2,r3)
    G=zip(g1,g2,g3)
    B=zip(b1,b2,b3)

    # transposing list
    RGB=zip(R,G,B)
    rgb=zip(*RGB)
    # print(rgb)

    # creating dictionary
    k=['red', 'green', 'blue']
    colourMap=dict(zip(k,rgb)) # makes a dictionary from 2 lists
    
    # Finally, register the colour maps
    mpl.cm.register_cmap(name=name, data=colourMap) #, lut=128)

# Load the colour maps in ColourMaps/
colourMapsDir = fn = os.path.join(os.path.dirname(__file__), "ColourMaps/")
for colourMap in glob.glob(colourMapsDir + "*_0-1.csv"):
    name = re.search("ColourMaps/(.*)_0-1.csv", colourMap)
    name = name.groups()[0]
    loadColourMap(name, colourMap)
    
#loadColourMap("cube1", "ColourMaps/cube1_0-1.csv")

################################################################################
# Change some of the matplotlib defaults

# http://matplotlib.org/users/customizing.html

# Thicker lines
mpl.rcParams['lines.linewidth'] = 2.0

# Larger fonts
mpl.rcParams['font.size'] = 16.0
mpl.rcParams['font.weight'] = 400 #550

# Make axes thicker
mpl.rcParams['axes.linewidth'] = 2.0

mpl.rcParams['xtick.major.width'] = 2.0
mpl.rcParams['xtick.major.size'] = 5.0
mpl.rcParams['xtick.minor.width'] = 2.0
mpl.rcParams['xtick.minor.size'] = 3.0

mpl.rcParams['ytick.major.width'] = 2.0
mpl.rcParams['ytick.major.size'] = 5.0
mpl.rcParams['ytick.minor.width'] = 2.0
mpl.rcParams['ytick.minor.size'] = 3.0

# Make axes labels bigger (relative to main font)
mpl.rcParams['axes.labelsize'] = 'large'
mpl.rcParams['axes.titlesize'] = 'x-large'
#mpl.rcParams['xtick.labelsize'] = 'large'
#mpl.rcParams['ytick.labelsize'] = 'large'

# Better figure layouts
mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['figure.figsize'] = [12.0, 9.0]

# Better images
mpl.rcParams['image.aspect'] = 'auto'
mpl.rcParams['image.interpolation'] = 'none'
#mpl.rcParams['image.cmap'] = 'cube1' # 'cubeYF' 'gist_earth' 'gnuplot2' 'YlGnBu_r'
# https://bids.github.io/colormap/
mpl.rcParams['image.cmap'] = 'viridis' # 'magma', 'inferno', 'plasma', 'viridis'

# Saving figures
mpl.rcParams['savefig.dpi'] = 500
mpl.rcParams['savefig.transparent'] = True
mpl.rcParams['savefig.frameon'] = True

################################################################################
# Pretty boxplot

def boxplot(x, labels=None):
    # Plotting properties
    boxprops     = dict(linewidth = 3.0, color = (0.3,)*3, zorder = 10)
    medianprops  = dict(linewidth = 3.0, color='red')
    whiskerprops = dict(linewidth = 3.0, linestyle='-', color=(0.7,)*3)
    capprops     = dict(linewidth = 3.0, linestyle='-', color=(0.7,)*3)
    flierprops   = dict(marker = 'o', markersize = 1, markeredgecolor = 'red', markerfacecolor = 'red', zorder = 0)
    
    # Draw the boxplot
    bp = plt.boxplot(x, labels=labels, notch=True, widths=0.65, boxprops=boxprops, medianprops=medianprops, whiskerprops=whiskerprops, capprops=capprops, flierprops=flierprops)
    
    # Horizontally jitter the fliers
    for i,f in enumerate(bp['fliers']):
        # Get extent of caps (there are 2 caps for each set of fliers)
        xLims = bp['caps'][2*i].get_xdata();
        xRange = xLims[1] - xLims[0]
        # Add some random jitter to the x-locations
        xd = f.get_xdata()
        f.set_xdata( xd + 0.8 * xRange * numpy.random.uniform(-0.5, 0.5, xd.shape) )
    
    return bp

################################################################################
# Image with better data cursor

# http://stackoverflow.com/a/27707723
class Formatter(object):
    def __init__(self, im):
        self.im = im
    def __call__(self, x, y):
        z = self.im.get_array()[int(y+0.5), int(x+0.5)]
        if numpy.isfinite(z):
            #return 'x={:.01f}, y={:.01f}, z={:.2e}'.format(x, y, z)
            if numpy.abs(z) < 0.01:
                return 'x={x:d}, y={y:d}, |z|<0.01'.format(x=int(x+0.5), y=int(y+0.5))
            else:
                return 'x={x:d}, y={y:d}, z={z:+.2f}'.format(x=int(x+0.5), y=int(y+0.5), z=z)
        else:
            return 'x={x:d}, y={y:d}, z=NaN'.format(x=int(x+0.5), y=int(y+0.5))


# Unfortunately, the format_cursor_data() API call doesn't seem to resolve the z-issue
# http://matplotlib.org/api/axes_api.html?highlight=format_coord#matplotlib.axes.Axes.format_cursor_data
#def xyFormat(x,y):
#    return 'x={x:d}, y={y:d}'.format(x=int(x+0.5), y=int(y+0.5))
#def zFormat(z):
#    return 'z={:.2f}'.format(z)

def imshow(X, *args, **kwargs):
    # Draw the image
    im = plt.imshow(X, *args, **kwargs)
    
    # Update the cursor format
    plt.gca().format_coord = Formatter(im)
    #plt.gca().format_coord = xyFormat
    #plt.gca().format_cursor_data = zFormat
    
    return im

################################################################################
# Draws a matrix

def plotMatrix(X, title=None, cmap=None, vmax=None, vmin=None, xlabel=None, ylabel=None, clabel=None, fileName=None):
    # Get data range
    if vmin is None:
        #vmin = - numpy.nanmax(numpy.abs(X))
        vmin = - numpy.nanpercentile(numpy.abs(X), 98.5)
    if vmax is None:
        #vmax = numpy.nanmax(numpy.abs(X))
        vmax = numpy.nanpercentile(numpy.abs(X), 98.5)
        
    # Plot matrix
    fig, ax = plt.subplots()
    im = imshow(X, vmax=vmax, vmin=vmin, cmap=cmap)
    cbar = plt.colorbar(im, ticks=mpl.ticker.MaxNLocator(nbins=6)) #, pad=0.025)
    
    # Labels
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if clabel is not None:
        cbar.set_label(clabel, rotation=270, labelpad=20)
    
    plt.tight_layout()
        
    # Save
    if fileName is not None:
        plt.savefig(fileName)
        
    # Title
    if title is not None:
        ax.set_title(title)
    
    # Return key objects
    return fig, ax, im, cbar

################################################################################
# Draws a correlation / covariance matrix

# Covariance - just a matrix with a blue/red colour map
def plotCovarianceMatrix(C, title=None, vmax=None, label=None, xlabel=None, ylabel=None, clabel="Covariance", fileName=None):
    # Labels
    if label is not None:
        xlabel = label; ylabel = label
    
    # Colour range - force to be symmetric around zero
    if vmax is not None:
        vmin = -vmax
    else:
        vmin = None
    
    # Pass to draw matrix
    return plotMatrix(C, title=title, cmap=mpl.cm.RdBu_r, vmax=vmax, vmin=vmin, xlabel=xlabel, ylabel=ylabel, clabel=clabel, fileName=fileName)


# Correlation - covariance between -1 and 1
def plotCorrelationMatrix(C, title=None, label=None, xlabel=None, ylabel=None, clabel="Correlation coefficient", fileName=None):
    
    # Pass to draw covariance matrix, changing limits
    return plotCovarianceMatrix(C, title=title, vmax=1.0, label=label, xlabel=xlabel, ylabel=ylabel, clabel=clabel, fileName=fileName)

################################################################################
