#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

################################################################################

# Generate some summary plots from a set of postprocessed PFMs

# Data location
#resultsDir = "~/DataSets/Andy/TonicPain/Results/Run_02.prof/Results_M50.pp/"
#N = 25

#resultsDir = "~/Andy/PINS/Results/Run_01.prof/PostprocessedResults/"
#N = 40

#resultsDir = "~/Liv/Results/s2_1_PP/"
#N = 40

resultsDir = "~/DataSets/Genie/Results/Results_M50.pp/"
N = 18

#resultsDir = "~/DataSets/T_Wass/Results/Results_M50_Dbetween.pp/"
#N = 26

#resultsDir = "~/DataSets/Matt_R/Results/PROFUMO/Results_M40_r3.pp/"
#N = 13

#resultsDir = "~/DataSets/Vishvarani/Results/Run_01.prof//Results_M50.pp/"
#N = 25

################################################################################
# Import

import os, os.path
import glob
from PROFUMO import makeDirectory as makeDir

import numpy, numpy.linalg, numpy.fft
import scipy, scipy.linalg
import random

import matplotlib as mpl
import matplotlib.pyplot as plt
import samplots as splt

import PROFUMO
import PFMplots

resultsDir = os.path.expanduser(resultsDir)

################################################################################
# Nicer display of netmat stats - display as a square

netmatStats = sorted(glob.glob(os.path.join(resultsDir, "NetMats/Statistics/Netmats_dat_tstat_fwep_c*.csv")))
for netmat in netmatStats:
    print(os.path.basename(netmat))
    pVals = 1 - numpy.loadtxt(netmat, delimiter=",")
    pSquare = numpy.diag(pVals[0:N])
    pInds = numpy.triu_indices(N,1)
    pSquare[pInds] = pVals[N:]
    pSquare[numpy.tril_indices(N,-1)] = -0.1
    #print(numpy.array_str(pSquare, max_line_width=1000000))
    # numpy.savetxt("temp.txt", pSquare)
    
    print("Max p: {:.3f}".format(numpy.max(pVals)))
    
    splt.plotCorrelationMatrix(pSquare, os.path.basename(netmat), clabel="1 - p")
    
    # And label key vals
    plt.hold(True)
    
    plt.gca().invert_yaxis()
    inds = numpy.where(pSquare > 0.95)
    plt.plot(inds[1], inds[0], 'w+', markersize=8.0, markeredgewidth=2.0, markeredgecolor='w')
    plt.xlim([-0.5, N-0.5]); plt.ylim([-0.5, N-0.5])
    plt.gca().invert_yaxis()

plt.show()

################################################################################
# Nicer display of weights

weightStats = sorted(glob.glob(os.path.join(resultsDir, "Weights/Statistics/Weights_dat_tstat_fwep_c*.csv")))
pWeights = numpy.zeros((len(weightStats), N))

for w,weights in enumerate(weightStats):
    print(os.path.basename(weights))
    pVals = 1 - numpy.loadtxt(weights, delimiter=",")
    
    print("Max p: {:.3f}".format(numpy.max(pVals)))
    
    pWeights[w,:] = pVals
    

# Plot!
splt.plotCorrelationMatrix(pWeights, "Weights")

# And label key vals
plt.hold(True)

plt.gca().invert_yaxis()
inds = numpy.where(pWeights > 0.95)
plt.plot(inds[1], inds[0], 'w+', markersize=8.0, markeredgewidth=2.0, markeredgecolor='w')
plt.xlim([-0.5, pWeights.shape[1]-0.5]); plt.ylim([-0.5, pWeights.shape[0]-0.5])
plt.gca().invert_yaxis()

plt.show()


