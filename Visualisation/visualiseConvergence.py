#!/usr/bin/env python

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# Looks at the convergence of a set of PFMs
import PROFUMO
import PFMplots

import os
import glob

import numpy, numpy.linalg
import scipy, scipy.linalg
import random

import matplotlib as mpl
import matplotlib.pyplot as plt
import samplots as splt

################################################################################
# Initialise PFMs

# Data location
#resultsDir = "~/scratch/PROFUMO/Test/"
#resultsDir = "~/Andy/PINS/Results/Test.pfm/Analysis/"
resultsDir = "/home/fs0/janineb/scratch/HCP/DMN1200/PFM_simulations/Results/PROFUMO_PFMsims_atlas_noOverlap15_vol.pfm"
resultsDir = os.path.expanduser(resultsDir)

pfms = PROFUMO.PFMs(resultsDir)

################################################################################
# Group maps

groupMaps = pfms.loadGroupSpatialMaps()

# Select a subset of modes
modes = numpy.where( numpy.std(groupMaps, axis=0) > 0.025 )[0]
modes = range(pfms.M)
M = len(modes)

PFMplots.plotMaps(groupMaps[:,modes], "Group maps", vmax=1.0, vmin=-1.0)

plt.show()

################################################################################
# Load all intermediates and plot rate of change

intermediates = pfms.loadIntermediates(modes=modes)

for model in sorted(intermediates):
    # Look at convergence to final set of maps
    end = max(intermediates[model].keys())
    target = {iteration: intermediates[model][end] for iteration in intermediates[model]}
    
    # Plot (taking into account which modes)!
    PFMplots.plotConvergence(intermediates[model], target, title=model)
    PFMplots.plotRateOfConvergence(intermediates[model], title=model)
    
    plt.show()

################################################################################
