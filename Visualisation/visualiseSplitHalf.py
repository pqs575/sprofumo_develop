#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# Looks at the convergence of a set of PFMs
import PROFUMO
import PFMplots

import os
import glob

import numpy, numpy.linalg
import scipy, scipy.linalg
import random
import itertools
import operator

import matplotlib as mpl
import matplotlib.pyplot as plt
import samplots as splt

################################################################################
# Initialise
print("*" * 80)
print("Initialising results directories...")
print()

# Data locations
# ONLY TWO FOR NOW!!!
#resultsDirectories = ["~/scratch/PROFUMO/S820_M50_Jun16.pfm/", 
#                      "~/scratch/PROFUMO/S820_M50_Jul16.pfm/"]
#resultsDirectories = ["~/Andy/PINS/Results/Run_01.pfm/Analysis/",
#                      "~/Andy/PINS/Results/Run_02.pfm/Analysis/"]
#resultsDirectories = ["~/scratch/PROFUMO/TemporalModelTests/Andy/NHRF1.pfm/",
#                      "~/scratch/PROFUMO/TemporalModelTests/Andy/NHRF2.pfm/"]
#resultsDirectories = ["~/Liv/Results/s2_1.pfm/",
#                      "~/Liv/Results/s2_2.pfm/"]
#resultsDirectories = ["/vols/Scratch/HCP/rfMRI/PROFUMO/TR1C_S100_M50.pfm",
#                      "/vols/Scratch/HCP/rfMRI/PROFUMO/TR2C_S100_M50.pfm"]
#resultsDirectories = ["~/DataSets/MEG/Results/Notts_P50.pfm/",
#                      "~/DataSets/MEG/Results/Notts_P50_2.pfm/"]
resultsDirectories = ["~/DataSets/Genie/Results/Analysis_M50_Test.pfm/",
                      "~/DataSets/Genie/Results/Analysis_M50_Test2.pfm/"]


saveResults = False

# Clean up directory names
resultsDirectories = [os.path.expanduser(rDir) for rDir in resultsDirectories]
N = len(resultsDirectories)

# Initialise PFMs
pfms = [PROFUMO.PFMs(rDir) for rDir in resultsDirectories]

print("Done.")
print()

################################################################################
# Group maps
print("*" * 80)
print("Examining final group maps...")
print()

# Load group maps
groupMaps = [pfm.loadGroupSpatialMaps() for pfm in pfms]
#for n in range(N):
#    PFMplots.plotMaps(groupMaps[n], "Group maps (set {:d})".format(n), vmax=1.0, vmin=-1.0)
#plt.show()

# Sizes
V,M = groupMaps[0].shape
modeOrders = [range(M) for groupMap in groupMaps]


# Pair up
# MAKE THIS GENERAL!!! SEE TEMPLATE MATCHING CODE FOR OLD 7T WORK
print("Pairing group maps...")
correlations, modeOrders[0], modeOrders[1] = PROFUMO.pairMaps(groupMaps[0], groupMaps[1])
print("Done.")
print()


# Select a subset of modes to examine further
print("Selecting a subset of modes...")
# Just set a minimum group map size
modesToKeep = [(numpy.std(maps[:,modes], axis=0) > 0.01) for maps,modes in zip(groupMaps,modeOrders)]
# http://stackoverflow.com/a/13783363
modesToKeep = [all(mode) for mode in zip(*modesToKeep)]
modesToKeep = [m for m,keep in enumerate(modesToKeep) if keep]
M = len(modesToKeep)

# Take this subset and re-order group maps
modeOrders = [[modes[m] for m in modesToKeep] for modes in modeOrders]
groupMaps = [maps[:,modes] for maps,modes in zip(groupMaps,modeOrders)]

# Put together all the matched maps
matchedMaps = numpy.zeros((V, N*M))
for n in range(N):
    matchedMaps[:,n::N] = groupMaps[n]

print("Done.")
print()


# Save some key results, if requested
if saveResults is True:
    print("Saving key results...")
    # Save paired maps
    numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", matchedMaps)
    
    # Save mode orders in results directories
    for n in range(N):
        numpy.savetxt(resultsDirectories[n] + "Modes.txt", modeOrders[n], fmt="%d")
    
    print("Done.")
    print()


# Plot!
print("Plotting group maps...")

PFMplots.plotMaps(matchedMaps, "Matched group maps ({:d} sets)".format(N), vmax=1.0, vmin=-1.0)
PFMplots.plotCorrelations(numpy.abs(correlations), "Split half", modes=modesToKeep)

plt.show()
print("Done.")
print()

################################################################################
# Load and plot the intermediates
print("*" * 80)
print("Loading intermediates...")
print()

# Load intermediates
intermediates = [pfm.loadIntermediates(modes=modes) for pfm,modes in zip(pfms,modeOrders)]

# Find common set of models
modelLists = [intermediate.keys() for intermediate in intermediates]
models = set.intersection( *[set(modelList) for modelList in modelLists] )
models = sorted(list( models ))

# Plot!
for model in models:
    print("*" * 80)
    print("Examining intermediates from {:s}...".format(model))
    print()
    
    # Plot (taking into account which modes)!
    print("Plotting intermediates...")
    for n1,n2 in itertools.combinations(range(N), 2):
        PFMplots.plotConvergence(intermediates[n1][model], intermediates[n2][model], "{m} ({n1:d}, {n2:d})".format(m=model, n1=n1, n2=n2))
    
    for n in range(N):
        PFMplots.plotRateOfConvergence(intermediates[n][model], "{m} ({n:d})".format(m=model, n=n))
    
    plt.show()
    print("Done.")
    print()

################################################################################

print("*" * 80)
