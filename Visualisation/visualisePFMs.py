#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

################################################################################

# Generate some summary plots from a set of PFMs

# Data location

resultsDir = "/home/fs0/janineb/scratch/HCP/DMN1200/PFM_simulations/Results/PROFUMO_PFMsims_atlas_noOverlap15_vol.pfm"

#resultsDir = "/vols/Scratch/HCP/rfMRI/PROFUMO/S1003_M50.pfm"
#resultsDir = "/vols/Scratch/HCP/rfMRI/PROFUMO/Matt_S449_M50.pfm/"
#resultsDir = "/vols/Scratch/HCP/rfMRI/PROFUMO/S10_M100ICA.pfm/"
#resultsDir = "~janineb/scratch/PROFUMO/PFM30_vol_820subs.pfm/"

#resultsDir = "/vols/Scratch/HCP/rfMRI/PROFUMO/S820_M25_ICAbasis.pfm"

#resultsDir = "~/DataSets/Andy/PINS/Results/Run_01.prof/Analysis_M50.pfm/"
#resultsDir = "~/DataSets/Andy/TonicPain/Results/Run_02.prof/Analysis_M50.pfm/"

#resultsDir = "~/DataSets/Liv/Results/Analysis_M30.pfm/"

#resultsDir = "~/DataSets/Genie/Results/Analysis_M50_Test.pfm/"

#resultsDir = "~/DataSets/T_Wass/Results/Analysis_M50.pfm/"

#resultsDir = "~/DataSets/MEG/Results/notts_ukmp/Notts_M75.pfm/"
#resultsDir = "~/DataSets/MEG/Results/robert_hcp/profumo/Analysis_M80.pfm"

#resultsDir = "/vols/Scratch/HCP/rfMRI/PROFUMO/BB/S50_M50.pfm"

#resultsDir = "~/DataSets/Sean/Results/Analysis_M40.pfm/"

#resultsDir = "~/scratch/grotgrot.pfm/"

#resultsDir = "~/DataSets/Vishvarani/Results/Run_01.prof/Analysis_M50.pfm"

#resultsDir = "~/DataSets/Matt_R/Results/PROFUMO/Analysis_M40.pfm/"

# Figures
#saveFigures = True
saveFigures = False
showFigures = True
#showFigures = False
plotSpatialBasis = True

################################################################################
# Import

import os, os.path
import glob
from PROFUMO import makeDirectory as makeDir

import numpy, numpy.linalg, numpy.fft
import scipy, scipy.linalg, scipy.stats
import random

import matplotlib as mpl
if showFigures is False:
    mpl.use('Agg')  # Backend that doesn't require Xserver
import matplotlib.pyplot as plt
import samplots as splt

import PROFUMO
import PFMplots

################################################################################
# Initialise PFMs
print("*" * 80)
print("Initialising...")
print()

# Clean up directories
resultsDir = os.path.expanduser(resultsDir)

# Make figure directories
if saveFigures is True:
    figuresBaseDir = os.path.join(resultsDir, "Figures"); makeDir(figuresBaseDir)
else:
    figuresDir = None
    fileName = None


# Load PFMs
pfms = PROFUMO.PFMs(resultsDir)

# Subset of subjects
subjects = pfms.subjects
if len(subjects) > 50:
    subjects = sorted(random.sample(pfms.subjects, 50))
S = len(subjects)

# Subset of modes
modesFile = os.path.join(resultsDir, "Modes.txt")
if os.path.exists(modesFile):
    modes = numpy.loadtxt(modesFile, dtype=int)
    M = len(modes)
    # CHECK THIS IS NOT EMPTY
else:
    M = pfms.M
    modes = range(M)
    #groupMaps = pfms.loadGroupSpatialMaps()
    #modes = numpy.where( numpy.std(groupMaps, axis=0) > 0.025 )[0]
    #M = len(modes)

'''
import scipy.cluster
pfms = PROFUMO.PFMs("/vols/Scratch/HCP/rfMRI/PROFUMO/S1003_M50.pfm/")
Pg = pfms.loadGroupSpatialMaps()
Z = scipy.cluster.hierarchy.linkage(Pg.T, method='single', metric='cosine')
zz = scipy.cluster.hierarchy.dendrogram(Z)
PFMplots.plotMaps(Pg[:,zz['leaves']], "Pg")
'''

#subjectMaps = pfms.loadSubjectSpatialMaps(subjects)
#GS_Corrs = PROFUMO.calculateGroupSubjectMapConsistencies(groupMaps, subjectMaps, subjects)
#fig, ax = plt.subplots()
#plt.plot(numpy.mean(GS_Corrs,axis=0))
#modes = numpy.where( (numpy.std(groupMaps, axis=0) > 0.025) & (numpy.mean(GS_Corrs,axis=0) > 0.5) )[0]

################################################################################
# Spatial basis
if plotSpatialBasis and (not "Fixed group parameters" in pfms.config):
    print("*" * 80)
    print("Examining spatial basis...")
    print()

    # Load basis
    print("Loading spatial basis...")
    SB = PROFUMO.loadHDF5(os.path.join(resultsDir, "Preprocessing/SpatialBasis.hdf5"))
    IM = PROFUMO.loadHDF5(os.path.join(resultsDir, "Preprocessing/InitialMaps.hdf5"))
    print("Done.")
    print()

    # Plot!
    print("Plotting spatial basis...")
    
    PFMplots.plotMaps(SB, "Spatial basis")
    PFMplots.plotMaps(IM, "Initial maps")
    
    if pfms.config["Multi-start iterations"] > 1:
        fileNames = glob.glob(os.path.join(resultsDir, "Preprocessing/BasisDecomposition/DecomposedSpatialBasis_*_Reordered.hdf5"))
        intermediates = [PROFUMO.loadHDF5(fn) for fn in fileNames]
        plt.figure(); plt.hold(True)
        similarity = numpy.zeros((len(fileNames), IM.shape[1]))
        for (i,inter) in enumerate(intermediates):
            similarity[i,:] = numpy.abs(PROFUMO.calculatePairedMapCorrelations(IM, inter[:,range(IM.shape[1])]))
            plt.plot(similarity[i,:])
        plt.plot(numpy.nanmean(similarity, axis=0), 'w', linewidth=7)
        plt.plot(numpy.nanmean(similarity, axis=0), 'k', linewidth=3)
        plt.ylim(0.0, 1.0); plt.ylabel("Cosine similarity"); plt.xlabel("Mode")
        plt.title("Similarity between final maps and intermediates")
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()

################################################################################
# Group maps
print("*" * 80)
print("Examining group maps...")
print()

# Load group maps
groupMaps = pfms.loadGroupSpatialMaps(modes)

# Set shapes
V = groupMaps.shape[0]

# Plot!
print("Plotting group maps...")
if saveFigures is True:
    figuresDir = os.path.join(figuresBaseDir, "GroupMaps"); makeDir(figuresDir)

PFMplots.plotMaps(groupMaps, "Group maps", directory=figuresDir)

# Look at the histograms
means = pfms.loadGroupSpatialMapMeans(modes)
probs = pfms.loadGroupSpatialMapMemberships(modes, applyNormalisation=False)

plt.figure(); plt.hist(means.flatten(), bins=99, range=(-3.0,3.0)); plt.hold(True)
import scipy.stats
x = numpy.linspace(-3.0, 3.0, 500)
#y = (5.0 / M) * scipy.stats.norm.pdf(x) * groupMaps.size
y = 0.1 * scipy.stats.norm.pdf(x, 0.5, 1.0) * groupMaps.size
plt.plot(x, y, 'r')

plt.figure(); plt.hist(probs.flatten(), bins=99, range=(0.0,1.0)); plt.hold(True)

plt.figure(); plt.plot(probs.flatten(), means.flatten(), '.', markersize=3)

H,x,y = numpy.histogram2d(means.flatten(), probs.flatten(), bins=255, range=((-2.0,2.0), (0.0,1.0)))
splt.plotMatrix( numpy.flipud(H), vmin=0.0, vmax=numpy.percentile(H, 99.5))

if showFigures is True:
    plt.show()
else:
    plt.close("all")
print("Done.")
print()



# numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", groupMaps)
# cd ..
# module load MATLAB/2014b && matlab -nojvm -nodisplay -r txt2cifti
"""
# HSL colourspace
import colorsys
H = numpy.linspace(0, (M-1)/M, M)
shuffle(H)
with open(os.path.expanduser("~/scratch/labels.txt"), 'w') as labels:
    for i,h in enumerate(H):
        (r, g, b) = colorsys.hls_to_rgb(h, 0.2 + 0.7*numpy.random.rand(), 0.7 + 0.3*numpy.random.rand())
        #(r, g, b) = colorsys.hls_to_rgb(numpy.random.rand(), 0.1 + 0.8*numpy.random.rand(), 0.5 + 0.5*numpy.random.rand())
        R, G, B = int(255 * r), int(255 * g), int(255 * b)
        labels.write("LABEL_{:d}\n".format(i+1))
        labels.write("{:d} {:d} {:d} {:d} 255\n".format(i+1,R,G,B))
"""

"""
# YUV
import random
# https://en.wikipedia.org/wiki/YUV#HDTV_with_BT.709
def yuv2rgb(y, u, v): # [0,1], [-1,1], [-1,1] --> [0,255],...
    u_max = 0.436; v_max = 0.615
    r = y + 1.280 * (v * v_max)
    g = y - 0.215 * (u * u_max) -  0.381 * (v * v_max)
    b = y + 2.128 * (u * u_max)
    return numpy.clip(r,0,1),  numpy.clip(g,0,1),  numpy.clip(b,0,1)

with open(os.path.expanduser("~/scratch/labels.txt"), 'w') as labels:
    for m in range(M):
        i = random.randint(0,3)
        #i = m % 4
        if i == 0: # U +ve
            u = 0.5 + 0.5*random.random(); v = 1.5 * random.random() - 1.0
        elif i == 1: # U -ve
            u = -0.5 - 0.5*random.random(); v = 1.5 * random.random() - 0.5
        elif i == 2: # V +ve
           u = 1.0 * random.random() - 1.0;  v = 0.5 + 0.5*random.random()
        elif i == 3: # V -ve
           u = 2.0 * random.random() - 1.0;  v = -0.5 - 0.5*random.random()
        else:
            print("Huh?")
        
        r,g,b = yuv2rgb(0.2 + 0.7*random.random(), u, v)
        R,G,B = int(255 * r), int(255 * g), int(255 * b)
        labels.write("LABEL_{:d}\n".format(m+1))
        labels.write("{:d} {:d} {:d} {:d} 255\n".format(m+1,R,G,B))
"""


"""
with open(os.path.expanduser("~/scratch/labels.txt"), 'w') as labels:
    for m in range(M):
        c = int(255 * random.random())
        labels.write("LABEL_{:d}\n".format(m+1))
        labels.write("{:d} {:d} {:d} {:d} 255\n".format(m+1,c,c,c))
"""

# parcellation = numpy.argmax(groupMaps, axis=1) + 1
# numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", parcellation)
# cd ~/DataSets/HCP/
# module load MATLAB/2014b && matlab -nojvm -nodisplay -r txt2cifti
# wb_command -cifti-label-import /vols/Scratch/samh/PROFUMO.dtseries.nii ~/scratch/labels.txt /vols/Scratch/samh/PROFUMO.dlabel.nii

### wb_command -cifti-label-import /vols/Scratch/samh/PROFUMO.dtseries.nii "" /vols/Scratch/samh/PROFUMO.dlabel.nii
### Make consistent across repeats with wb_command -cifti-label-export-table
### wb_command -cifti-label-export-table ~/scratch/PROFUMO.dlabel.nii 1 ~/scratch/labels.txt

# subject = pfms.subjects[1000]
# subject = '201111'
# subjectMaps = pfms.loadSubjectSpatialMaps([subject,], modes)
# parcellation = numpy.argmax(subjectMaps[subject], axis=1) + 1
# THEN SAVE AGAIN!
# wb_command -cifti-label-import /vols/Scratch/samh/PROFUMO.dtseries.nii ~/scratch/labels.txt /vols/Scratch/samh/PROFUMO.dlabel.nii



#Q = PROFUMO.loadHDF5(pfmsDir + "/GroupSpatialMaps.post/VoxelwisePrecisions.post/Means.hdf5")
#Q = 1 / numpy.sqrt(Q[:,modes])
#PFMplots.plotMaps(Q/5.0, "Q", vmax=1.0, vmin=-1.0)

#maps = numpy.zeros((V, 2*M))
#maps[:,0::2] = groupMaps
#maps[:,1::2] = Q/5.0
#PFMplots.plotMaps(maps, "PQ", vmax=1.0, vmin=-1.0)
#numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", maps)

# Save group maps
#templateCIFTI = nibabel.nifti2.load("/home/fs0/samh/scratch/YeoRSNs.dtseries.nii")
#groupMapCIFTI = nibabel.nifti2.Nifti2Image(numpy.reshape(groupMaps.T, (1,1,1,1,M,V)), templateCIFTI.affine, templateCIFTI.header, templateCIFTI.extra)
#groupMapCIFTI.header.set_data_shape( (1,1,1,1,M,V) )
#nibabel.nifti2.save(groupMapCIFTI, "/home/fs0/samh/scratch/PROFUMO.dtseries.nii")
# Still isn't setting XML properly :-(

# http://humanconnectome.org/documentation/workbench-command/command-all-commands-help.html

################################################################################
# Subject maps
print("*" * 80)
print("Examining subject maps...")
print()

subjectMaps = pfms.loadSubjectSpatialMaps(subjects, modes)

if pfms.config["Spatial model"] == "Modes":
    # Plot some examples!
    print("Plotting example subject maps...")
    
    lim = numpy.percentile(numpy.abs(subjectMaps[subjects[0]]), 98.5)
    
    # Plot the group maps again, but now with a colour scale to match the subject maps
    #PFMplots.plotMaps(groupMaps, "Group maps", vmax=lim, vmin=-lim, directory=figuresDir)
    
    # Plot an example subject
    for subject in random.sample(subjects, 1):
        if saveFigures is True:
            figuresDir = os.path.join(figuresBaseDir, "SubjectMaps", subject); makeDir(figuresDir)
        PFMplots.plotMaps(PROFUMO.interleaveMaps([groupMaps, subjectMaps[subject]]), "Group & subject " + subject + " spatial maps", vmax=lim, vmin=-lim, directory=figuresDir)
    
    # Plot an example set of mode maps
    for m in random.sample(range(M), 1):
        # Extract maps from each subject
        modeMaps = numpy.zeros((V,S+1))
        modeMaps[:,0] = groupMaps[:,m]
        for s,subject in enumerate(subjects):
            modeMaps[:,s+1] = subjectMaps[subject][:,m]
        # Plot
        if saveFigures is True:
            figuresDir = os.path.join(figuresBaseDir, "ModeMaps", str(m)); makeDir(figuresDir)
        PFMplots.plotMaps(modeMaps, "Mode {:d} spatial maps".format(m), vmax=lim, vmin=-lim, xlabel="Subject", directory=figuresDir)
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()
    
    
    # Calculate consistency
    print("Calculating subject map consistencies...")
    
    GS_Corrs = PROFUMO.calculateGroupSubjectMapConsistencies(groupMaps, subjectMaps, subjects)
    SS_Corrs = PROFUMO.calculateSubjectMapConsistencies(subjectMaps, subjects)
    
    print("Done.")
    print()
    
    
    # Look at consistency
    print("Plotting subject map consistencies...")
    
    if saveFigures is True:
        figuresDir = os.path.join(figuresBaseDir, "MapConsistencies"); makeDir(figuresDir)
    
    if saveFigures is True:
        fileName = os.path.join(figuresDir, "GroupSubject.png")
    splt.plotCorrelationMatrix(GS_Corrs, title="Group-subject map similarities", xlabel="Mode", ylabel="Subject", fileName=fileName)
    
    if saveFigures is True:
        fileName = os.path.join(figuresDir, "SubjectSubject.png")
    splt.plotCorrelationMatrix(SS_Corrs, title="Subject-subject map similarities", xlabel="Mode", ylabel="Subject-subject pair", fileName=fileName)
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()



elif pfms.config["Spatial model"] == "Parcellation":
    # Plot some examples!
    print("Plotting example subject parcellations...")
    
    # Turn into a "find the biggest" parcellation
    parcellations = numpy.zeros((V, S+1))
    
    parcellations[:,0] = numpy.argmax(groupMaps, axis=1)
    
    for s,subject in enumerate(subjects):
        parcellations[:,s+1] = numpy.argmax(subjectMaps[subject], axis=1)
    
    # Plot!
    if saveFigures is True:
        figuresDir = os.path.join(figuresBaseDir, "SubjectParcellations"); makeDir(figuresDir)
    PFMplots.plotMaps(parcellations, "Parcellations", vmin=0, vmax=M-1, directory=figuresDir)
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()
    
    
    # Look at consistency
    print("Plotting subject parcellation consistencies...")
    
    consistency = numpy.zeros((S+1, S+1))
    
    # Group - subject
    for s in range(1, S+1):
        consistency[0,s] = numpy.mean(parcellations[:,0] == parcellations[:,s])
    
    
    for s1 in range(1, S+1):
        for s2 in range(s1+1, S+1):
            consistency[s1,s2] = numpy.mean(parcellations[:,s1] == parcellations[:,s2])
    
    consistency = consistency + consistency.T; numpy.fill_diagonal(consistency, 1.0)
    
    # Plot!
    if saveFigures is True:
        fileName = os.path.join(figuresDir, "Consistencies.png")
    splt.plotCorrelationMatrix(consistency, title="Parcellation consistencies", xlabel="Subject", ylabel="Subject", fileName=fileName)
    
    
    # Look at how consistent parcel labels are on a voxel-by-voxel basis
    voxelwiseConsistency = numpy.mean(parcellations[:,[0,]] == parcellations[:,1:], axis=1)
    plt.figure()
    plt.plot(voxelwiseConsistency, '.', markersize=3)
    plt.xlim(0,V); plt.ylim(-0.05, 1.05); plt.xlabel("Voxel"); plt.ylabel("Consistency")
    
    # And look at this with entropy
    probs = pfms.loadGroupSpatialMapMemberships()
    voxelwiseEntropy = numpy.zeros((V,))
    for v in range(V):
        voxelwiseEntropy[v] = scipy.stats.entropy(probs[v,:])
    
    plt.figure()
    plt.plot(voxelwiseEntropy, '.', markersize=3)
    plt.xlim(0,V); plt.xlabel("Voxel"); plt.ylabel("Entropy")
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()


# numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", numpy.vstack([voxelwiseConsistency,voxelwiseEntropy]).T)

'''
import nibabel

groupMaps = pfms.loadGroupSpatialMaps(modes)
groupParcels = numpy.argmax(groupMaps, axis=1)
groupMeans = pfms.loadGroupSpatialMapMeans(modes)

subject = '201111'
subjectMaps = pfms.loadSubjectSpatialMaps([subject,], modes)[subject]
subjectParcels = numpy.argmax(subjectMaps, axis=1)
subjectMeans = PROFUMO.loadHDF5(resultsDir + "/FinalModel/Subjects/" + subject + "/SpatialMaps.post/Means.hdf5")

D = nibabel.load("/vols/Data/HCP/Phase2/subjects1200/" + subject + "/MNINonLinear/Results/rfMRI_REST1_LR/rfMRI_REST1_LR_Atlas_MSMAll_hp2000_clean.dtseries.nii")
D = D.get_data().squeeze().T
D = (D.T - numpy.mean(D, axis=1)).T

norm = PROFUMO.loadHDF5(resultsDir + "/Preprocessing/201111/1_LR_Normalisation.hdf5")
D = D * norm

groupCorrs = []; subjectCorrs = []
groupVE = []; subjectVE = []
for m in range(M):
    print(m)
    groupVoxels = (groupParcels == m)
    groupTCs = groupMeans[groupVoxels, m] @ D[groupVoxels, :]
    groupCorrs.extend( numpy.corrcoef(groupTCs, D[groupVoxels, :])[1,:] )
    u,s,v = numpy.linalg.svd(D[groupVoxels, :], full_matrices=False)
    groupVE.append( s[0]**2 / numpy.sum(s**2) )
    
    subjectVoxels = (subjectParcels == m)
    subjectTCs = subjectMeans[subjectVoxels, m] @ D[subjectVoxels, :]
    subjectCorrs.extend( numpy.corrcoef(subjectTCs, D[subjectVoxels, :])[1,:] )
    u,s,v = numpy.linalg.svd(D[subjectVoxels, :], full_matrices=False)
    subjectVE.append( s[0]**2 / numpy.sum(s**2) )

plt.figure(); plt.hist(groupCorrs, bins=199)
plt.figure(); plt.hist(subjectCorrs, bins=199)

plt.scatter(groupVE, subjectVE)
plt.hold(True); plt.plot([0,0.5], [0,0.5], 'k')

'''

################################################################################
# Time courses
print("*" * 80)
print("Examining time courses...")
print()

# Load TCs
timeCourses = pfms.loadSubjectTimeCourses(subjects, modes)


# Plot some examples!
print("Plotting example time courses...")

for subject in random.sample(subjects, 1):
    run = random.sample(timeCourses[subject].keys(),1)[0]
    
    # Standard time courses
    TCs = timeCourses[subject][run]
    if saveFigures is True:
        figuresDir = os.path.join(figuresBaseDir, "TimeCourses", subject, run); makeDir(figuresDir)
    PFMplots.plotTimeCourses(TCs, "Time courses (subject " + subject + ", " + run + ")", directory=figuresDir)

if showFigures is True:
    plt.show()
else:
    plt.close("all")
print("Done.")
print()


# Plot clean HRF TCs
if "HRF" in pfms.config["Temporal model"]:
    # Load Clean TCs
    cleanTCs = pfms.loadSubjectTimeCourses(subjects=[subject,], modes=modes, cleanTCs=True)
    cleanTCs = cleanTCs[subject][run]
    T = cleanTCs.shape[1]
    TCs = pfms.loadSubjectTimeCourses(subjects=[subject,], modes=modes, cleanTCs=False)
    TCs = TCs[subject][run]

    #print("Loading HRF...")
    hrfFile = os.path.join(resultsDir, "FinalModel", "GroupTemporalModel", "HRFCovariance_T" + str(T) + ".hdf5")
    K = PROFUMO.loadHDF5(hrfFile)
    K = 0.99 * K + 0.01 * numpy.eye(T)
    L = numpy.linalg.cholesky(K)  #K = scipy.linalg.fractional_matrix_power(K, -0.5)
    #print("Done.")
    #print()
    
    # And after decorrelation
    K_TCs = numpy.linalg.solve(L, cleanTCs.T).T  #K_TCs = numpy.dot(cleanTCs, K)
    if saveFigures is True:
        figuresDir = os.path.join(figuresDir, "Decorrelated"); makeDir(figuresDir)
    PFMplots.plotTimeCourses(K_TCs, "Decorrelated TCs (subject " + subject + ", " + run + ")", directory=figuresDir)
    
    # Plot a random time course
    m = random.sample(range(M),1)[0]
    fig, ax = plt.subplots()
    plt.hold(True)
    plt.plot( TCs[m,:], 'g', label='Time course' )
    plt.plot( cleanTCs[m,:], 'b', label='Clean time course' )
    plt.plot( K_TCs[m,:], 'r', label='Decorrelated TC' )
    ax.legend()
    ax.set_xlabel("Time point")
    if saveFigures is True:
        fileName = os.path.join(figuresDir, "Comparison.eps")
        plt.savefig(fileName)
    ax.set_title("Raw and decorrelated TCs (subject " + subject + ", " + run + ", mode " + str(modes[m]) + ")")
    
    # Plot clean and noisy side-by-side
    T = TCs.shape[1]
    combinedTCs = numpy.zeros((2*M, T))
    combinedTCs[0::2, :] = cleanTCs
    combinedTCs[1::2, :] = TCs
    
    PFMplots.plotTimeCourses(combinedTCs, "Combined time courses (subject " + subject + ", " + run + ")", directory=figuresDir)
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()


# Look at noisy TCs
if os.path.isdir(str(pfms.directory) + "/FinalModel/Subjects/" + subject + "/Runs/" + run + "/TimeCourses.post/Noise"):
    print("Looking at noisy time courses...")
    
    # Load noisy TCs
    noiseTCs = PROFUMO.loadHDF5(str(pfms.directory) + "/FinalModel/Subjects/" + subject + "/Runs/" + run + "/TimeCourses.post/Noise/TimeCourses/Means.hdf5")
    noiseTCs = noiseTCs[modes,:]
    
    # Plot!
    vmax = numpy.percentile(numpy.abs(TCs), 99.5)
    vmin = -vmax
    PFMplots.plotTimeCourses(TCs, "Time courses (subject " + subject + ", " + run + ")", vmax=vmax, vmin=vmin)
    PFMplots.plotTimeCourses(cleanTCs, "Clean time courses (subject " + subject + ", " + run + ")", vmax=vmax, vmin=vmin)
    PFMplots.plotTimeCourses(noiseTCs, "Noise time courses (subject " + subject + ", " + run + ")", vmax=vmax, vmin=vmin)
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()

'''
# ONLY FOR SCANS OF THE SAME LENGTH!!!
T = TCs.shape[1]

# Incrementally calculate mean and variance of FFTs
mFFT = numpy.zeros((M,T)); vFFT = numpy.zeros((M,T)); N = 0
for subject in subjects:
    for run in timeCourses[subject]:
        TCs = timeCourses[subject][run]
        fft = numpy.abs(numpy.fft.fft(TCs, axis=1))
        #K_TCs = numpy.dot(TCs, K)
        #fft = numpy.abs(numpy.fft.fft(K_TCs, axis=1))
        
        # Incremental updates
        N = N + 1
        delta = fft - mFFT
        mFFT = mFFT + delta / float(N)
        vFFT = vFFT + delta * (fft - mFFT)

vFFT = vFFT / float(N-1)

fig, ax = plt.subplots()
im = splt.imshow( mFFT, vmin=0.0 )
cbar = plt.colorbar(im)
ax.set_xlabel("FFT"); ax.set_ylabel("Mode")
ax.set_title("mFFT")
ax.set_xlim(0,T/2)

fig, ax = plt.subplots()
im = splt.imshow( vFFT, vmin=0.0 )
cbar = plt.colorbar(im)
ax.set_xlabel("FFT"); ax.set_ylabel("Mode")
ax.set_title("vFFT")
ax.set_xlim(0,T/2)

if showFigures is True:
    plt.show()
'''

#m = random.sample(range(M),1)[0]
#f = linspace(0, 1.0/0.72, T+1); f = f[0:-1]
#fig, ax = plt.subplots()
#plt.hold(True)
#plt.fill_between(f, mFFT[m,:] - numpy.sqrt(vFFT[m,:]), mFFT[m,:] + numpy.sqrt(vFFT[m,:]), color=(0.8,)*3, linewidth=0.0)
#plt.plot(f, mFFT[m,:], 'r', linewidth=4.0)
#ax.set_xlim(0,0.69); ax.set_ylim(bottom=0)
#ax.set_yticks([]); ax.set_ylabel("FFT")
#ax.set_xlabel("Frequency (Hz)")
#ax.set_title("Mode {:d}".format(modes[m]))

# LOOK AT GAMMA DISTRIBUTION FOR ERRORBARS?
#a = (mFFT[m,:] ** 2) / vFFT[m,:]
#b = mFFT[m,:] / vFFT[m,:]
#x = numpy.linspace(0,25,500)
#y = scipy.stats.gamma.cdf(x, a[10], scale=1.0/b[10])
#fig, ax = plt.subplots()
#plt.plot(x,y)

#import scipy.interpolate
#scipy.interpolate.interp1d(y, x)([0.05, 0.95])

################################################################################
# Weightings
print("*" * 80)
print("Examining component weightings...")
print()

# Load weights
weights = pfms.loadSubjectWeights(subjects, modes)

# Work out equivalent(ish) quantities from the maps and time courses
print("Calculating variances...")

R = 0
for subject in subjects:
    R = R + len(weights[subject])

P = numpy.zeros((R,M)); H = numpy.zeros((R,M)); A = numpy.zeros((R,M)); V = numpy.zeros((R,M));
r = 0
for subject in subjects:
    for run in weights[subject]:
        # Maps
        P[r,:] = numpy.std(subjectMaps[subject], axis=0)
        
        # Weights
        H[r,:] = weights[subject][run]
        
        # (Noisy) time courses
        A[r,:] = numpy.std(timeCourses[subject][run], axis=1)
        
        # Overall (clean) variance
        V[r,:] = numpy.sqrt( numpy.mean(subjectMaps[subject] ** 2, axis=0)
                             * (weights[subject][run] ** 2) 
                             * numpy.mean(TCs ** 2, axis=1)
                           )
        
        r = r + 1

print("Done.")
print()

# Plot some examples!
print("Plotting variances...")

if saveFigures is True:
    figuresDir = os.path.join(figuresBaseDir, "Weightings", "P"); makeDir(figuresDir)
PFMplots.plotMaps(P, "P", ylabel="Run", directory=figuresDir)

if saveFigures is True:
    figuresDir = os.path.join(figuresBaseDir, "Weightings", "H"); makeDir(figuresDir)
PFMplots.plotMaps(H, "H", ylabel="Run", directory=figuresDir)

if saveFigures is True:
    figuresDir = os.path.join(figuresBaseDir, "Weightings", "A"); makeDir(figuresDir)
PFMplots.plotMaps(A, "A", ylabel="Run", directory=figuresDir)

if saveFigures is True:
    figuresDir = os.path.join(figuresBaseDir, "Weightings", "Combined"); makeDir(figuresDir)
PFMplots.plotMaps(V, "Combined", ylabel="Run", directory=figuresDir)

if showFigures is True:
    plt.show()
else:
    plt.close("all")
print("Done.")
print()

################################################################################
# Temporal precision matrices
print("*" * 80)
print("Examining temporal correlations...")
print()

# Group precision matrix
modelType, groupPrecMats = pfms.loadGroupPrecisionMatrices(modes)

# Subject precision matrices
_, subjectPrecMats = pfms.loadSubjectPrecisionMatrices(subjects, modes)


# Plot
print("Plotting example precision matrices...")
if saveFigures is True:
    figuresDir = os.path.join(figuresBaseDir, "GroupPrecMats"); makeDir(figuresDir)
for modelClass in groupPrecMats:
    if saveFigures is True:
        figuresDir = os.path.join(figuresDir, modelClass); makeDir(figuresDir)

    PFMplots.plotPrecisionMatrix(groupPrecMats[modelClass], "Group precision matrix: " + modelClass, directory=figuresDir)
    
    if saveFigures is True:
        figuresDir = os.path.dirname(figuresDir)

    #for subject in random.sample(subjects, 2):
    #    if saveFigures is True:
    #        figuresDir = os.path.join(figuresBaseDir, "SubjectPrecMats", subject); makeDir(figuresDir)
    #    PFMplots.plotPrecisionMatrix(subjectPrecMats[subject], "Subject " + subject + " precision matrix", directory=figuresDir)

if showFigures is True:
    plt.show()
else:
    plt.close("all")
print("Done.")
print()


# Look at consistency over subjects
if modelType == "Subject":
    print("Converting to netmats...")
    
    netMatElems = numpy.zeros((S+1, int(M*(M-1)/2)))
    # Group first
    netMatElems[0,:], _ = PROFUMO.convertPrecMatToNetMatElems( groupPrecMats["All runs"] )
    # Then subjects
    for s,subject in enumerate(subjects):
        netMatElems[s+1,:], _ = PROFUMO.convertPrecMatToNetMatElems( subjectPrecMats[subject] )
    
    print("Done.")
    print()
    
    
    # Plot
    print("Plotting netmats...")
    if saveFigures is True:
        figuresDir = os.path.join(figuresBaseDir, "NetMats"); makeDir(figuresDir)
    
    PFMplots.plotNetMatElems(netMatElems, "Netmats", directory=figuresDir)
    
    if showFigures is True:
        plt.show()
    else:
        plt.close("all")
    print("Done.")
    print()

################################################################################
print("*" * 80)
raise SystemExit()

