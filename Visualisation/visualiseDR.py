import PROFUMO, PFMplots, numpy
import random

resultsDir = "/vols/Scratch/HCP/rfMRI/PROFUMO/S820_M25_ICAbasis.pfm"
pfms = PROFUMO.PFMs(resultsDir)

Pg = PROFUMO.loadHDF5(resultsDir + "/GroupMapMeans.hdf5")

subjects = random.sample(pfms.subjects, 50)
subjectMaps = pfms.loadSubjectSpatialMaps(subjects)

mPs = numpy.zeros((91282,25))
for P in subjectMaps.values():
    mPs = mPs + P
mPs = mPs / 50

P = numpy.zeros((91282,100))
P[:,0::4] = Pg
P[:,1::4] = mPs
P[:,2::4] = subjectMaps[subjects[12]]
P[:,3::4] = subjectMaps[subjects[32]]

PFMplots.plotMaps(P, "Group maps")

numpy.savetxt("/vols/Scratch/samh/PROFUMO.txt", P)
