%%

addpath('~steve/matlab/')

%EigS=sqrt(sum(MIGP.cdata.^2))';       % get sqrt(eigenvalues)
   % the above in your case would be "s" from svd or sqrt(d) from eigs
EigS = sqrt(C);
   
EigD=EigS.^2; EigD=EigD/max(EigD);    % get normalised eigenvalues
EigDn1=round(length(EigD)*0.6);
EigDn2=round(length(EigD)*0.8);
g1=10000; g3=100000;                  % golden search for best-fitting spatial-DoF
for i=1:20
  g2=(g1+g3)/2;
  EigNull=iFeta([0:0.0001:5],round(1.05*length(EigD)),g2)';
  EigNull=EigNull*sum(EigD(EigDn2:EigDn2+10))/sum(EigNull(EigDn2:EigDn2+10));
  grot=EigD(1:EigDn1)-EigNull(1:EigDn1);
  if min(grot)<0,  g1=g2;  else,  g3=g2;  end;
  [i g1 g2 g3]
end
%%
EigDc=EigD-EigNull(1:length(EigD));  % subtract null eigenspectrum
grot=smooth(EigDc(50:end),50,'loess'); i=min(find(abs(grot)<1e-5))+50-10;
EigDc(i:end)= (1-(1:(1+length(EigDc)-i))/(1+length(EigDc)-i)).^2 * grot(i-50);
  subplot(1,2,1);   hold off; plot(EigNull  ); hold on; plot(EigD,'g'); plot(EigDc,'r'); hold off;
EigSc=sqrt(abs(EigDc)); EigScn=(EigSc>0).*EigSc./(EigS/EigS(1));  % get correction factors
  %subplot(1,3,2); plot([EigScn  EigSc grot400 grot400.*EigS/EigS(1) grot4000 grot4000.*EigS/EigS(1)]);
  subplot(1,2,2); plot([ EigS EigS.*EigScn]);
%%
% below is my final recon of the dense connectome - you would be doing something else
BOdconn.cdata= MIGP.cdata * diag( EigScn.^2 ) * MIGP.cdata';