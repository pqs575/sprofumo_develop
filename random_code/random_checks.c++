// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - test stuff 
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */
// run like this:  
// $ cd /vols/Scratch/rezvanh/sPFM_Dev_jalapeno/random_code/
// $ scl enable devtoolset-6 bash
// $ export LD_LIBRARY_PATH=/home/fs0/rezvanh/lib_jalapeno
// line below or just $make (it will update the modified files)
// $ make unistall then $ make clean then $ make then $ make all then $ make install
// ./random_checks /home/fs0/rezvanh/HCP_Profumo_cifti_goodsubs200/DataLocations3.json /vols/Scratch/rezvanh/test_rebuilding/ 20 -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain_mask_dil1.nii.gz

#include <map>
#include <memory>
#include <string>
#include <iostream>
#include <iomanip>    // std::put_time
#include <fstream>
#include <sstream>    // std::ostringstream
#include <sys/stat.h>
#include <locale>     // std::locale, std::isspace, std::isdigit
#include <armadillo>
#include <experimental/filesystem>



//#include "/home/fs0/rezvanh/PROFUMO/Source/Utilities/DataIO.h"


int main() {
    int subNum = 11;
    int batchSize = 4;
    float thisRatio = (float)subNum / (float) batchSize;
    std::cout << thisRatio << std::endl;
    arma::fvec V_S_R;
    //V_S_R.load("/vols/Scratch/rezvanh/HCP_Profumo_cifti_bigdata_lr21_subin.pfm/BigDataInitialDir/N_VSR.hdf5", arma::hdf5_binary);
    //int subjectNum = V_S_R.at(1);
    //std::cout << subjectNum << std::endl;
    int rhoCnt = 10;
    int rhoAlpha = 1;
    float rhoBeta = -0.1;
    float currentRho;
    rhoCnt++;
    currentRho = std::pow((rhoCnt+rhoAlpha),rhoBeta);
    std::cout << currentRho << std::endl;
    std::vector<float> logDetX;
    //std::ifstream fileLogDet;   
    //fileLogDet.open("/vols/Scratch/rezvanh/HCP_Profumo_cifti2_goodsubs200_freeSNR.pfm/FinalModel/Subjects/100408/Runs/R1_LR/NoisePrecision.post/GammaPosterior.txt");
    std::ifstream myfile("/vols/Scratch/rezvanh/HCP_Profumo_cifti2_goodsubs200_freeSNR.pfm/FinalModel/Subjects/100408/Runs/R1_LR/NoisePrecision.post/GammaPosterior.txt", std::ios_base::in);
    std::string astr, bstr;
    float a, b;

    myfile >> astr >> a >> bstr >> b;
    
    
    arma::fvec agPrior(2);
    agPrior.at(0) = a;
    agPrior.at(1) = b;
    
    std::cout << a/b << std::endl;
    
    arma::fmat Ad;
    //Ad.load("/vols/Scratch/rezvanh/HCP_Profumo_cifti2nd_bigdata_200subs_subin_M150.pfm/BigDataInitialDir/Subjects/589567/Runs/R2_LR/RunPreprocessed_Ad.hdf5", arma::hdf5_binary);
    
    //Ad.save("/vols/Scratch/rezvanh/HCP_Profumo_cifti2nd_bigdata_200subs_subin_M150.pfm/BigDataInitialDir/Subjects/589567/Runs/R2_LR/RunPreprocessed_Ad.bin", arma::arma_binary);
    
    //arma::fmat Ad_txt;
    //Ad_txt.load("/vols/Scratch/rezvanh/HCP_Profumo_cifti2nd_bigdata_200subs_subin_M150.pfm/BigDataInitialDir/Subjects/589567/Runs/R2_LR/RunPreprocessed_Ad.bin");
    
    //std::cout << (Ad - Ad_txt).max() << std::endl;
    //std::cout << (Ad_txt).max() << std::endl;
    //std::cout << (Ad_txt).min() << std::endl;
    
    //arma::fmat APtest = PROFUMO::Utilities::loadDataMatrix("/vols/Scratch/rezvanh/HCP_Profumo_cifti2_bigdata_200subs_subin_M150.pfm/BigDataInitialDir/Subjects/589567/Runs/R2_LR/RunPreprocessed_Ad.bin");
    
    //std::vector<std::string> subjectNames = {"770352", "196346", "141422", "173940", "789373", "250427", "135629", "380036", "177746", "151324", "346945", "275645", "223929", "102816", "202113", "517239", "531940", "227432", "188549", "139839", "111211", "134829", "732243", "393550", "308331", "194443", "188347", "200917", "148335", "200311", "562345", "816653", "199352", "135730", "187345", "238033", "406836", "870861", "138534", "947668", "668361", "154532", "599065", "561949", "212217", "120414", "170631", "205119", "176744", "759869", "672756", "270332", "809252", "130114", "112112", "512835", "573249", "169949", "191336", "397760", "103515", "912447", "695768", "131823", "567052", "927359", "346137", "179245", "413934", "136126", "161327", "530635", "749058", "120111", "724446", "845458", "589567", "167238", "397861", "465852", "536647", "888678", "149337", "171330", "195647", "453542", "263436", "322224", "849264", "368551", "185947", "181131", "105923", "901038", "329844", "211417", "185341", "180937", "169343", "117021", "654552", "578057", "692964", "187850", "136833", "157336", "877168", "818859", "436239", "130922", "153631", "108121", "186848", "547046", "100408", "106016", "660951", "771354", "113619", "199453", "877269", "176239", "213017", "192843", "214625", "557857", "690152", "153227", "421226", "966975", "257845", "571144", "206727", "108828", "126325", "856968", "105115", "212015", "873968", "802844", "211114", "156233", "910241", "172433", "210011", "163331", "208125", "896778", "285345", "965771", "176037", "101006", "156334", "391748", "702133", "561242", "151526", "283543", "580751", "147030", "677766", "849971", "251833", "686969", "932554", "192237", "586460", "583858", "123824", "118831", "361941", "111514", "654754", "620434", "529953", "599671", "141826", "627852", "820745", "753150", "137431", "191235", "121618", "609143", "167743", "194140", "723141", "480141", "182840", "162329", "204420", "565452", "349244", "207426", "467351", "706040", "144428", "680250", "204521", "350330"};
    //std::string inputDir = "/vols/Scratch/rezvanh/HCP_Profumo_cifti2nd_bigdata_200subs_subin_M150.pfm/BigDataInitialDir/Subjects/";
    //std::vector<std::string> runNames = {"R1_LR","R1_RL","R2_LR","R2_RL"};
    //for (unsigned int s = 0; s < 200; ++s) {
    //    for (unsigned int r = 0; r < 4; ++r) {
    //        std::string inputFilePd = inputDir + subjectNames[s] + "/Runs/" + runNames[r] + "/RunPreprocessed_Pd.bin";
    //        std::string inputFileAd = inputDir + subjectNames[s] + "/Runs/" + runNames[r] + "/RunPreprocessed_Ad.bin";
    //        std::string inputFileN = inputDir + subjectNames[s] + "/Runs/" + runNames[r] + "/RunPreprocessed_NTrDtD.bin";
    //        arma::fmat Pd_bin;
    //        arma::fmat Ad_bin;
    //        arma::fmat Nd_bin;
    //        Pd_bin.load(inputFilePd);
    //        std::cout << subjectNames[s] + runNames[r] + " Pd" << std::endl;
    //        std::cout << Pd_bin.n_cols << std::endl;
    //        Ad_bin.load(inputFileAd);
    //       std::cout << subjectNames[s] + runNames[r] + " Ad" << std::endl;
    //        std::cout << Ad_bin.n_cols << std::endl;
    //        Nd_bin.load(inputFileN);
    //        std::cout << subjectNames[s] + runNames[r] + " Nd" << std::endl;
    //        std::cout << Nd_bin.n_cols << std::endl;
    //    }
    //}
        
    

    //getchar();
    //read a float from a text file
    //float ldx;
    //fileLogDet >> ldx;
    //std::cout << ldx << std::endl;
    //fileLogDet.close();
    
    //float gammaPrior[2];
    //int num = 0; // num must start at 0
    //for(num=0;num<2;++num){
    //    fileLogDet >> gammaPrior[num]; // read first column number
    //    std::cout << num << std::endl;
  
    //}           
    
    //fileLogDet.close();
    //std::cout << gammaPrior[0] << std::endl;
    //std::cout << gammaPrior[1] << std::endl;
    
    std::vector<arma::fmat> variantCollection;
    arma::fmat A(5,10,arma::fill::zeros);
    arma::fmat B(2,10,arma::fill::zeros);
    arma::fvec y = arma::zeros<arma::fvec>(3);
    //std::vector<bool> C = std::vector<bool>(6, true);
    variantCollection.emplace_back(A);
    variantCollection.emplace_back(B);
    variantCollection.emplace_back(y);
    //variantCollection.emplace_back(C);
    //std::cout << variantCollection[0] << std::endl;
    
    std::shared_ptr<std::vector<arma::fmat>> expectationInitialisation = std::make_shared<std::vector<arma::fmat>>(variantCollection);
    std::vector<arma::fmat> thisMat = *expectationInitialisation;
    //std::cout << thisMat[0] << std::endl;
    
    std::vector<bool> initBool = {false, false};
    std::cout << initBool[0] << std::endl;
    
    arma::fmat testEmpty;
    if (testEmpty.is_empty()){
        std::cout << "yes it's empty!" << std::endl;
    }
    
    std::vector<float> interimLogp;
    arma::fvec log_p = arma::zeros<arma::fvec>(5);
    for (unsigned int c = 0; c < 5; ++c) {
        interimLogp.push_back(0.0);
        log_p[c]=0.0004;
    }
    interimLogp.clear();
    for (unsigned int c = 0; c < 5; ++c) {
        interimLogp.push_back(log_p[c]);
        log_p[c]=3.0;
        std::cout << interimLogp[c] << std::endl;
    }
    std::cout << log_p << std::endl;
    
    arma::fmat Xvec = arma::randu<arma::fmat>(4,5);
    arma::fmat Yvec = arma::randu<arma::fmat>(4,5);
    //Yvec.at(1,2)=0.2;
    //Yvec.at(2,3)=0.3;
    
    arma::frowvec subjectGroupCorr = arma::zeros<arma::frowvec>(Xvec.n_cols);
    for (unsigned int cnt1=0; cnt1<Xvec.n_cols; ++cnt1) {
        arma::fmat thisCorr = arma::cor(Xvec.col(cnt1), Yvec.col(cnt1));
        std::cout << thisCorr << std::endl;
        subjectGroupCorr.at(cnt1)=thisCorr.at(0);
    }
    std::cout << subjectGroupCorr << std::endl;

    arma::fmat spatialSigns = arma::sign(subjectGroupCorr);
    
    std::cout << spatialSigns << std::endl;
    
    std::cout << "Deleting preprocessed data from disk" << std::endl;
    const std::string toRemove_directory= "/vols/Scratch/rezvanh/PROFUMO_simulations/latest/PFM_Simulations-master/Results_classic_stochastic_misal_500subs_batchSize/PROFUMO_PFMsims_stochastic2_atlas_Simulations_oldbasis_nsim02_nG05.pfm/BigDataInitialDir/Subjects";
    std::experimental::filesystem::remove_all(toRemove_directory);
    
    return 0;
}

// const arma::fmat maskData = PROFUMO::Utilities::loadDataMatrix( args.maskFile );
// options.maskInds = std::make_shared<const arma::uvec>(arma::find( maskData != 0.0 )); 
// run like this:  
// cd to ~rezvanh/PROFUMO
// $ scl enable devtoolset-6 bash
// $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/fs0/rezvanh/lib
// line below or just $make (it will update the modified files)
// $ make unistall then $ make clean then $ make then $ make all then $ make install
// ./main_test /home/fs0/rezvanh/HCP_Profumo_cifti_goodsubs200/DataLocations1.json /vols/Scratch/rezvanh/test_rebuilding 20 -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain_mask_dil1.nii.gz

 

 