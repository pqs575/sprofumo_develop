
// c++11 "g++ -O3 -Wall -Wextra -I/home/fs0/samh/include Test2.c++ -o Test2 -L/home/fs0/samh/lib -larmadillo -lhdf5"

// /usr/bin/time ./Test2.o

//#define ARMA_EXTRA_DEBUG 1
#include <iostream>
#include <armadillo>
#include <ctime>
#include <cmath>
#include <vector>
#include <string>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <sys/stat.h>


int main(int argc, char** argv)
{
    
    arma::arma_rng::set_seed_random();  // set the seed to a random value
    
    
    {
        //Generate some random data
        int V = 91282;
        int T = 1200;
        int M = 100;
        
        arma::mat D = arma::randn<arma::mat>(V,T);
        
        arma::mat P = arma::randu<arma::mat>(V,M);
        
        for (int i = 0; i < V; i++) {
            for (int j = 0; j < M; j++) {
                if( P(i,j) < 0.0222 ){
                    P(i,j) = 0;
                }
            }
        }
        
        

          
        arma::mat P2;
        P2.load("/vols/Scratch/samh/WeirdArmaMat/PositiveDeltaProbabilities.txt");
        
        
        arma::mat P3 = P2 + 1e-100;// * arma::randu<arma::mat>(V,M);
        P3 -= 1e-100;
        
        
        arma::mat P4 = P2;
        P4.elem( find(arma::abs(P4) < 1.0e-10) ).zeros();
        
        
        
        std::cout << arma::min(arma::min(P)) << "," << arma::max(arma::max(P)) << "," <<  arma::sum(arma::sum(P == 0)) << std::endl;
        std::cout << arma::min(arma::min(P2)) << "," << arma::max(arma::max(P2)) << ","<< arma::sum(arma::sum(P2 == 0)) << std::endl;
        std::cout << arma::min(arma::min(P3)) << "," << arma::max(arma::max(P3)) << "," << arma::sum(arma::sum(P3 == 0)) << std::endl;
        std::cout << arma::min(arma::min(P4)) << "," << arma::max(arma::max(P4)) << "," << arma::sum(arma::sum(P4 == 0)) << std::endl;

        
        clock_t begin, end;
        double timeElapsed;
        
        
        arma::mat PtD = arma::zeros<arma::mat>(V,T);
        begin = clock();
        PtD = P.t() * D;
        end = clock();
        timeElapsed = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Time elapsed: " << timeElapsed << "s" << std::endl << std::endl;
        

        
        
        arma::mat P2tD = arma::zeros<arma::mat>(V,T);
        begin = clock();
        P2tD = P2.t() * D;
        end = clock();
        timeElapsed = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Time elapsed: " << timeElapsed << "s" << std::endl << std::endl;
        
               
               
               
        arma::mat P3tD = arma::zeros<arma::mat>(V,T);
        begin = clock();
        P3tD = P3.t() * D;
        end = clock();
        timeElapsed = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Time elapsed: " << timeElapsed << "s" << std::endl << std::endl;
        
               
               
               
        arma::mat P4tD = arma::zeros<arma::mat>(V,T);
        begin = clock();
        P4tD = P4.t() * D;
        end = clock();
        timeElapsed = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Time elapsed: " << timeElapsed << "s" << std::endl << std::endl;
        
        
    }
    
    return 0;
}
